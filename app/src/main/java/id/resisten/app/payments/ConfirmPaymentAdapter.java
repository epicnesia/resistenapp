package id.resisten.app.payments;

import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;

import id.resisten.app.R;
import id.resisten.app.orders.OrdersDataModel;
import id.resisten.app.retrofit.APIUtils;

/**
 * Created by LEKTOP on 11-May-17.
 */

public class ConfirmPaymentAdapter extends RecyclerView.Adapter<ConfirmPaymentAdapter.ViewHolder> {

    private Context context;
    private List<OrdersDataModel> ordersList;

    private DecimalFormat IndonesianCurrency;
    private DecimalFormatSymbols formatRp;

    public ConfirmPaymentAdapter(Context context, List<OrdersDataModel> ordersList){
        this.context = context;
        this.ordersList = ordersList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from( parent.getContext() ).inflate( R.layout.fragment_confirm_payment_rv, parent, false );
        return new ConfirmPaymentAdapter.ViewHolder( itemView );
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        IndonesianCurrency = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol( "Rp. " );
        formatRp.setMonetaryDecimalSeparator( ',' );
        formatRp.setGroupingSeparator( '.' );

        IndonesianCurrency.setDecimalFormatSymbols( formatRp );

        if(!ordersList.get( position ).getProductImage().equals( "null" )){
            Glide.with( context ).load( APIUtils.BASE_URL + "/show_image/" + ordersList.get( position ).getProductImage() )
                    .into( holder.ivPaymentThumbnail );
        }
        final String orderId = "#" + ordersList.get( position ).getId();
        holder.tvPaymentOrderId.setText( orderId );
        holder.tvPaymentProductName.setText( ordersList.get( position ).getProductName() );
        holder.tvPaymentGrandTotal.setText( IndonesianCurrency.format( ordersList.get( position ).getGrandTotal() ) );

        holder.btPaymentDetail.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent( context, ConfirmPaymentDetailActivity.class );
                intent.putExtra( "orderId", ordersList.get( position ).getId() + "" );
                intent.putExtra( "sellerId", ordersList.get( position ).getSellerId() + "" );
                intent.putExtra( "productName", ordersList.get( position ).getProductName() );
                intent.putExtra( "sellerName", ordersList.get( position ).getSellerName() );
                intent.putExtra( "count", ordersList.get( position ).getCount() + "" );
                intent.putExtra( "totalWeight", ordersList.get( position ).getTotalWeight() + " g" );
                intent.putExtra( "totalPrice", IndonesianCurrency.format( ordersList.get( position ).getTotalPrice() ) );
                intent.putExtra( "totalShippingCost", IndonesianCurrency.format( ordersList.get( position ).getShippingCost() ) );
                intent.putExtra( "grandTotal", IndonesianCurrency.format( ordersList.get( position ).getGrandTotal() ) );
                context.startActivity( intent );

            }
        } );
    }

    @Override
    public int getItemCount() {
        return ordersList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ConstraintLayout clPayment;
        public ImageView ivPaymentThumbnail;
        public TextView tvPaymentOrderId;
        public TextView tvPaymentProductName;
        public TextView tvPaymentGrandTotal;
        public Button btPaymentDetail;

        public ViewHolder(View itemView) {
            super( itemView );

            clPayment           = (ConstraintLayout) itemView.findViewById( R.id.clPayment );
            ivPaymentThumbnail  = (ImageView) itemView.findViewById( R.id.ivPaymentThumbnail );
            tvPaymentOrderId    = (TextView) itemView.findViewById( R.id.tvPaymentOrderId );
            tvPaymentProductName= (TextView) itemView.findViewById( R.id.tvPaymentProductName );
            tvPaymentGrandTotal = (TextView) itemView.findViewById( R.id.tvPaymentGrandTotal );
            btPaymentDetail     = (Button) itemView.findViewById( R.id.btPaymentDetailButton );
        }
    }

}
