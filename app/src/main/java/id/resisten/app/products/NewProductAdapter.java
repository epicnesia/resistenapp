package id.resisten.app.products;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import id.resisten.app.R;
import id.resisten.app.orders.OrdersActivity;
import id.resisten.app.retrofit.APIUtils;
import id.resisten.app.users.CircleTransform;
import id.resisten.app.users.SessionManager;

import static android.content.ContentValues.TAG;

/**
 * Created by LEKTOP on 11-Apr-17.
 */

class NewProductAdapter extends RecyclerView.Adapter<NewProductAdapter.ViewHolder> {

    // Session Manager class
    private SessionManager session;
    private HashMap<String, String> user;

    private Context context;
    private List<NewProductModel> productList;

    private DecimalFormat IndonesianCurrency;
    private DecimalFormatSymbols formatRp;

    public NewProductAdapter(Context context, List<NewProductModel> productList) {
        session = new SessionManager(context);
        this.context = context;
        this.productList = productList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from( parent.getContext() ).inflate( R.layout.fragment_new_product_card, parent, false );
        return new ViewHolder( itemView );
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        user = session.getUserDetails();

        IndonesianCurrency = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol( "Rp. " );
        formatRp.setMonetaryDecimalSeparator( ',' );
        formatRp.setGroupingSeparator( '.' );

        IndonesianCurrency.setDecimalFormatSymbols( formatRp );

        if(!productList.get( position ).getSupplierPropict().equals("null")){
            Glide.with( context ).load( APIUtils.BASE_URL + "/show_image/" + productList.get( position ).getSupplierPropict() )
                    .crossFade()
                    .thumbnail( 0.5f )
                    .bitmapTransform( new CircleTransform( context ) )
                    .diskCacheStrategy( DiskCacheStrategy.ALL )
                    .into( holder.supplierPropict );
        }

        holder.supplierName.setText( productList.get( position ).getSupplierName() );
        holder.createdDate.setText( formatDateFromString( "yyyy-MM-dd HH:mm:ss", "dd-MMM-yyyy HH:mm:ss", productList.get( position ).getCreatedDate() ) );

        Glide.with( context ).load( APIUtils.BASE_URL + "/show_image/" + productList.get( position ).getImage() )
                .into( holder.thumbnail );

        holder.productName.setText( productList.get( position ).getName() );
        holder.productPrice.setText( IndonesianCurrency.format( productList.get( position ).getPrice() ) );
        holder.productCommision.setText( IndonesianCurrency.format( productList.get( position ).getCommision() ) );

        if( !user.get(SessionManager.KEY_ID).equals( Integer.toString( productList.get( position ).getSupplierId() ) ) ){

            holder.btEdit.setVisibility(View.GONE);
            holder.btFav.setVisibility(View.VISIBLE);

            holder.btFav.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.btFav.setImageResource( R.mipmap.ic_star_yellow_48dp );
                }
            } );

            holder.thumbnail.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent producDetailsIntent = new Intent(context, ProductDetailsActivity.class);
                    producDetailsIntent.putExtra( "suppPropict", productList.get( position ).getSupplierPropict() );
                    producDetailsIntent.putExtra( "suppName", productList.get( position ).getSupplierName() );
                    producDetailsIntent.putExtra( "provinceID", productList.get( position ).getProvinceId() + "" );
                    producDetailsIntent.putExtra( "cityID", productList.get( position ).getCityId() + "" );
                    producDetailsIntent.putExtra( "subdistrictID", productList.get( position ).getSubdistrictId() + "" );
                    producDetailsIntent.putExtra( "stock", productList.get( position ).getStock() + "" );
                    producDetailsIntent.putExtra( "productDate", formatDateFromString( "yyyy-MM-dd HH:mm:ss", "dd-MMM-yyyy", productList.get( position ).getCreatedDate() ) );
                    producDetailsIntent.putExtra( "productId", productList.get( position ).getId() + "" );
                    producDetailsIntent.putExtra( "productName", productList.get( position ).getName() );
                    producDetailsIntent.putExtra( "productPrice", productList.get( position ).getPrice() + "" );
                    producDetailsIntent.putExtra( "productPriceFormatted", IndonesianCurrency.format( productList.get( position ).getPrice() ) );
                    producDetailsIntent.putExtra( "productCommision", productList.get( position ).getCommision() + "" );
                    producDetailsIntent.putExtra( "productCommisionFormatted", IndonesianCurrency.format( productList.get( position ).getCommision() ) );
                    producDetailsIntent.putExtra( "productWeight", productList.get( position ).getWeight() + "" );
                    producDetailsIntent.putExtra( "productThumb", productList.get( position ).getImage() );
                    producDetailsIntent.putExtra( "productDescription", productList.get( position ).getDescription() );
                    context.startActivity( producDetailsIntent );

                }
            } );

        } else if( user.get(SessionManager.KEY_ID).equals( Integer.toString( productList.get( position ).getSupplierId() ) ) ){

            holder.btEdit.setVisibility(View.VISIBLE);
            holder.btFav.setVisibility(View.GONE);

        }

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView supplierPropict;
        public TextView supplierName;
        public TextView createdDate;
        public ImageView thumbnail;
        public TextView productName;
        public TextView productPrice;
        public TextView productCommision;
        public ImageView btFav;
        public ImageView btEdit;

        public ViewHolder(View itemView) {
            super( itemView );

            supplierPropict = (ImageView) itemView.findViewById( R.id.ivNewProductSuppPropict );
            supplierName    = (TextView) itemView.findViewById( R.id.tvNewProductSuppName );
            createdDate     = (TextView) itemView.findViewById( R.id.tvNewProductDate );
            thumbnail       = (ImageView) itemView.findViewById( R.id.newProductThumb );
            productName     = (TextView) itemView.findViewById( R.id.newProductName );
            productPrice    = (TextView) itemView.findViewById( R.id.newProductPrice );
            productCommision= (TextView) itemView.findViewById( R.id.newProductCommision );
            btFav           = (ImageView) itemView.findViewById( R.id.ivNewProductFavButton );
            btEdit          = (ImageView) itemView.findViewById( R.id.ivNewProductEditButton );
        }
    }

    public static String formatDateFromString(String inputFormat, String outputFormat, String inputDate){

        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);

        } catch (ParseException e) {
            Log.e(TAG, "ParseException - dateFormat");
        }

        return outputDate;

    }
}
