package id.resisten.app.orders;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.List;

import id.resisten.app.R;
import id.resisten.app.payments.ConfirmPaymentAdapter;
import id.resisten.app.payments.ConfirmPaymentDetailActivity;
import id.resisten.app.retrofit.APIServices;
import id.resisten.app.retrofit.APIUtils;
import id.resisten.app.shipments.ShipmentDetailsActivity;
import id.resisten.app.users.SessionManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Top on 23-May-17.
 */

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {

    // Session Manager class
    private SessionManager session;
    private HashMap<String, String> user;

    private Context context;
    private List<OrdersDataModel> ordersList;

    private DecimalFormat IndonesianCurrency;
    private DecimalFormatSymbols formatRp;

    public OrderAdapter(Context context, List<OrdersDataModel> ordersList) {
        session = new SessionManager(context);
        this.context = context;
        this.ordersList = ordersList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from( parent.getContext() ).inflate( R.layout.fragment_order_rv, parent, false );
        return new OrderAdapter.ViewHolder( itemView );
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        user = session.getUserDetails();

        IndonesianCurrency = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol( "Rp. " );
        formatRp.setMonetaryDecimalSeparator( ',' );
        formatRp.setGroupingSeparator( '.' );

        IndonesianCurrency.setDecimalFormatSymbols( formatRp );

        if(!ordersList.get( position ).getProductImage().equals( "null" )){
            Glide.with( context ).load( APIUtils.BASE_URL + "/show_image/" + ordersList.get( position ).getProductImage() )
                    .into( holder.ivOrderListThumbnail );
        }
        final String orderId = "#" + ordersList.get( position ).getId();
        holder.tvOrderListOrderId.setText( orderId );
        holder.tvOrderListProductName.setText( ordersList.get( position ).getProductName() );
        holder.tvOrderListGrandTotal.setText( IndonesianCurrency.format( ordersList.get( position ).getGrandTotal() ) );
        holder.btOrderListDetail.setText(ordersList.get( position ).getShippingStatus());

        holder.clOrderList.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent( context, OrdersListDetailsActivity.class );
                intent.putExtra( "orderId", ordersList.get( position ).getId() + "" );
                intent.putExtra( "productName", ordersList.get( position ).getProductName() );
                intent.putExtra( "sellerName", ordersList.get( position ).getSellerName() );
                intent.putExtra( "buyerName", ordersList.get( position ).getBuyerName() );
                intent.putExtra( "description", ordersList.get( position ).getDescription() );
                intent.putExtra( "count", ordersList.get( position ).getCount() + "" );
                intent.putExtra( "totalWeight", ordersList.get( position ).getTotalWeight() + " g" );
                intent.putExtra( "totalPrice", IndonesianCurrency.format( ordersList.get( position ).getTotalPrice() ) );
                intent.putExtra( "totalShippingCost", IndonesianCurrency.format( ordersList.get( position ).getShippingCost() ) );
                intent.putExtra( "grandTotal", IndonesianCurrency.format( ordersList.get( position ).getGrandTotal() ) );
                intent.putExtra( "shippingAgents", ordersList.get( position ).getShippingAgents() );
                intent.putExtra( "shippingServiceCode", ordersList.get( position ).getShippingServiceCode() );
                intent.putExtra( "shippingName", ordersList.get( position ).getShippingName() );
                intent.putExtra( "shippingPhone", ordersList.get( position ).getShippingPhone() );
                intent.putExtra( "shippingProvince",  ordersList.get( position ).getShippingProvinceName() );
                intent.putExtra( "shippingCity",  ordersList.get( position ).getShippingCityName() );
                intent.putExtra( "shippingSubdistrict",  ordersList.get( position ).getShippingSubdistrictName() );
                intent.putExtra( "shippingAddress", ordersList.get( position ).getShippingAddress() );
                intent.putExtra( "shippingStatus", ordersList.get( position ).getShippingStatus() );
                context.startActivity( intent );

            }
        } );

        if(!ordersList.get( position ).getShippingStatus().equals("DELIVERED")){
            holder.btOrderListDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    final ProgressDialog spinner = new ProgressDialog( context );
                    spinner.setMessage( context.getResources().getString( R.string.pleaseWait ) );
                    spinner.setCancelable( false );
                    spinner.show();

                    HashMap<String, String> params = new HashMap<>();
                    params.put( "api_token", user.get( SessionManager.KEY_TOKEN ) );
                    params.put( "order_id", ordersList.get( position ).getId() + "" );

                    APIServices apiServices     = APIUtils.getAPIService();
                    Call<ResponseBody> result   = apiServices.updateOrderStatus(params);
                    result.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                            try {

                                spinner.dismiss();

                                JSONObject jsonResponse = new JSONObject( response.body().string() );
                                boolean status          = jsonResponse.getBoolean( "status" );
                                String message          = jsonResponse.getString( "message" );
                                String shippingStatus   = jsonResponse.getString( "shipping_status" );

                                if(status){

                                    holder.btOrderListDetail.setText(shippingStatus);

                                } else {

                                    AlertDialog.Builder builder = new AlertDialog.Builder( context );
                                    builder.setMessage( message )
                                            .setNegativeButton( "Retry", null )
                                            .create()
                                            .show();

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            spinner.dismiss();
                            t.printStackTrace();
                        }
                    });

                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return ordersList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ConstraintLayout clOrderList;
        public ImageView ivOrderListThumbnail;
        public TextView tvOrderListOrderId;
        public TextView tvOrderListProductName;
        public TextView tvOrderListGrandTotal;
        public Button btOrderListDetail;

        public ViewHolder(View itemView) {
            super(itemView);

            clOrderList           = (ConstraintLayout) itemView.findViewById( R.id.clOrderList );
            ivOrderListThumbnail  = (ImageView) itemView.findViewById( R.id.ivOrderListThumbnail );
            tvOrderListOrderId    = (TextView) itemView.findViewById( R.id.tvOrderListOrderId );
            tvOrderListProductName= (TextView) itemView.findViewById( R.id.tvOrderListProductName );
            tvOrderListGrandTotal = (TextView) itemView.findViewById( R.id.tvOrderListGrandTotal );
            btOrderListDetail     = (Button) itemView.findViewById( R.id.btOrderListDetailButton );
        }

    }
}
