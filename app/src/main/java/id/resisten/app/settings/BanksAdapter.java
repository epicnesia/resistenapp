package id.resisten.app.settings;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.ResponseCache;
import java.util.HashMap;
import java.util.List;

import id.resisten.app.MainActivity;
import id.resisten.app.R;
import id.resisten.app.retrofit.APIServices;
import id.resisten.app.retrofit.APIUtils;
import id.resisten.app.suppliers.SuppliersAdapter;
import id.resisten.app.users.SessionManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Top on 22-May-17.
 */

public class BanksAdapter extends RecyclerView.Adapter<BanksAdapter.ViewHolder> {

    private Context context;
    private List<BanksModel> banksList;

    // Session Manager class
    private SessionManager session;
    private HashMap<String, String> user;

    public BanksAdapter(Context context, List<BanksModel> banksList) {
        this.context = context;
        this.banksList = banksList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from( parent.getContext() ).inflate( R.layout.fragment_setting_bank_rv, parent, false );
        return new BanksAdapter.ViewHolder( itemView );
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        // Session manager instance
        session = new SessionManager(context);
        user = session.getUserDetails();

        holder.tvBankName.setText(banksList.get(position).getBankName());
        holder.tvAccountNumber.setText(banksList.get(position).getAccountNumber());
        holder.tvAccountName.setText(banksList.get(position).getAccountName());

        holder.btDelete.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final ProgressDialog spinner = new ProgressDialog( context );
                spinner.setMessage( context.getResources().getString( R.string.pleaseWait ) );
                spinner.setCancelable( false );
                spinner.show();

                AlertDialog.Builder builder = new AlertDialog.Builder( context );
                builder.setMessage( R.string.deleteBankConfirm )
                        .setNegativeButton( "No", null )
                        .setPositiveButton( "Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                APIServices apiServices     = APIUtils.getAPIService();
                                Call<ResponseBody> result   = apiServices.delBank( user.get( SessionManager.KEY_TOKEN ), banksList.get( position ).getId() );
                                result.enqueue( new Callback<ResponseBody>() {
                                    @Override
                                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                                        spinner.dismiss();

                                        try {

                                            JSONObject jsonResponse = new JSONObject( response.body().string() );
                                            boolean status          = jsonResponse.getBoolean( "status" );
                                            String message          = jsonResponse.getString( "message" );

                                            if(status) {

                                                Toast.makeText( context, message, Toast.LENGTH_LONG ).show();

                                                banksList.remove(position);
                                                notifyItemRemoved(position);
                                                notifyItemRangeChanged(position, banksList.size());
                                                notifyDataSetChanged();

                                            } else {

                                                AlertDialog.Builder builder = new AlertDialog.Builder( context );
                                                builder.setMessage( message )
                                                        .setNegativeButton( "Retry", null )
                                                        .create()
                                                        .show();

                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }

                                    }

                                    @Override
                                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                                        spinner.dismiss();
                                        t.printStackTrace();
                                    }
                                } );
                            }
                        } )
                        .create()
                        .show();
            }
        } );
    }

    @Override
    public int getItemCount() {
        return banksList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvBankName,
                tvAccountNumber,
                tvAccountName;
        public ImageView btDelete;

        public ViewHolder(View itemView) {
            super(itemView);

            tvBankName      = (TextView) itemView.findViewById(R.id.tvBankName);
            tvAccountNumber = (TextView) itemView.findViewById(R.id.tvAccountNumber);
            tvAccountName   = (TextView) itemView.findViewById(R.id.tvAccountName);
            btDelete        = (ImageView) itemView.findViewById(R.id.ivBankDeleteButton);

        }
    }
}
