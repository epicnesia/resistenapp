package id.resisten.app.resellers;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import id.resisten.app.R;
import id.resisten.app.retrofit.APIServices;
import id.resisten.app.retrofit.APIUtils;
import id.resisten.app.suppliers.SupplierDetailsActivity;
import id.resisten.app.suppliers.SupplierDetailsModel;
import id.resisten.app.users.CircleTransform;
import id.resisten.app.users.LoginActivity;
import id.resisten.app.users.SessionManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class ResellerDetailsActivity extends AppCompatActivity {

    // Session Manager class
    private SessionManager session;
    private HashMap<String, String> user;

    private int page;

    private ProgressBar progressBar;

    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private ResellerDetailsAdapter resellerDetailsAdapter;
    private List<ResellerDetailsModel> resellersList;

    private ImageView ivResellerPropict;

    private TextView tvResellerName,
            tvResellerPhone,
            tvResellerEmail;

    private Button btResellerChat;

    private int id;

    private String resellerName,
            resellerPhone,
            resellerEmail,
            resellerPropict;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_reseller_details );

        // Session manager instance
        session = new SessionManager( ResellerDetailsActivity.this );
        if (!session.isLoggedIn()) {
            Intent loginIntent = new Intent( ResellerDetailsActivity.this, LoginActivity.class );
            ResellerDetailsActivity.this.startActivity( loginIntent );
            ResellerDetailsActivity.this.finish();
        }

        user = session.getUserDetails();

        getSupportActionBar().setTitle( getResources().getString( R.string.resellerDetailTitle ) );
        getSupportActionBar().setDisplayHomeAsUpEnabled( true );

        page = 1;

        // Initializing UI
        init_UI();

        // Setup reseller detail preview
        setup_reseller_detail();

        // Set orders list detail
        recyclerView    = (RecyclerView) findViewById( R.id.rvResellerOrderList );
        resellersList   = new ArrayList<>();

        setup_orders_list(id, page);

        layoutManager   = new LinearLayoutManager( ResellerDetailsActivity.this );
        recyclerView.setLayoutManager( layoutManager );

        resellerDetailsAdapter = new ResellerDetailsAdapter( ResellerDetailsActivity.this, resellersList );
        recyclerView.setAdapter( resellerDetailsAdapter );

    }

    // Initializing UI
    private void init_UI() {

        ivResellerPropict       = (ImageView) findViewById( R.id.ivResellerPropict );

        tvResellerName          = (TextView) findViewById( R.id.tvResellerName );
        tvResellerPhone         = (TextView) findViewById( R.id.tvResellerPhone );
        tvResellerEmail         = (TextView) findViewById( R.id.tvResellerEmail );

        btResellerChat          = (Button) findViewById( R.id.btResellerChat );

        progressBar             = (ProgressBar) findViewById( R.id.pbResellerDetails );

        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            id              = Integer.parseInt( extras.getString( "reseller_id" ) );
            resellerName    = extras.getString( "reseller_name" );
            resellerPhone   = extras.getString( "reseller_phone" );
            resellerEmail   = extras.getString( "reseller_email" );
            resellerPropict = extras.getString( "reseller_propict" );

        }

    }

    // Setup reseller detail preview
    private void setup_reseller_detail() {

        if(!resellerPropict.equals( "null" )) {
            Glide.with( ResellerDetailsActivity.this ).load( APIUtils.BASE_URL + "/show_image/" + resellerPropict )
                    .crossFade()
                    .thumbnail( 0.5f )
                    .bitmapTransform( new CircleTransform( ResellerDetailsActivity.this ) )
                    .diskCacheStrategy( DiskCacheStrategy.ALL )
                    .into( ivResellerPropict );
        }

        tvResellerName.setText( resellerName );
        tvResellerPhone.setText( resellerPhone );
        tvResellerEmail.setText( resellerEmail );

    }

    // Set orders list detail
    private void setup_orders_list(final int id, final int page) {

        AsyncTask<Integer, Void, Void> task = new AsyncTask<Integer, Void, Void>() {
            @Override
            protected Void doInBackground(Integer... params) {

                HashMap<String, String> param = new HashMap<>();
                param.put( "api_token", user.get( SessionManager.KEY_TOKEN ) );
                param.put( "page", Integer.toString( page ) );

                APIServices apiServices = APIUtils.getAPIService();
                Call<ResponseBody> result = apiServices.getUserRelationsDetails( id, param );
                try {

                    Response<ResponseBody> response = result.execute();

                    JSONObject jsonResponse = new JSONObject( response.body().string() );
                    boolean status = jsonResponse.getBoolean( "status" );

                    if(status){

                        JSONObject message  = jsonResponse.getJSONObject( "message" );
                        JSONObject orders   = message.getJSONObject( "orders" );

                        JSONArray array = orders.getJSONArray( "data" );
                        for(int i=0;i<array.length();i++){

                            JSONObject object       = array.getJSONObject( i );
                            JSONObject objectSeller = object.getJSONObject( "seller" );
                            JSONObject objectProduct= object.getJSONObject( "product" );

                            ResellerDetailsModel data = new ResellerDetailsModel(
                                    object.getInt( "id" ),
                                    object.getInt( "grand_total" ),
                                    object.getString( "created_at" ),
                                    objectSeller.getString( "name" ),
                                    objectProduct.getString( "name" ),
                                    object.getString( "shipping_status" )
                            );

                            resellersList.add( data );

                        }

                    } else {
                        String message = jsonResponse.getString( "message" );

                        AlertDialog.Builder builder = new AlertDialog.Builder( ResellerDetailsActivity.this );
                        builder.setMessage( message )
                                .setNegativeButton( "Retry", null )
                                .create()
                                .show();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid){
                progressBar.setVisibility( View.GONE );
                resellerDetailsAdapter.notifyDataSetChanged();
            }
        };

        task.execute();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
