package id.resisten.app.spinner;

/**
 * Created by LEKTOP on 24-Apr-17.
 */

public class CityDataModel {

    private int id, provinceId;
    private String province, type, cityName, postalCode;

    public CityDataModel(int id, int provinceId, String province, String type, String cityName, String postalCode) {
        this.id = id;
        this.provinceId = provinceId;
        this.province = province;
        this.type = type;
        this.cityName = cityName;
        this.postalCode = postalCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(int provinceId) {
        this.provinceId = provinceId;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    @Override
    public String toString() {
        return type + " " + cityName;
    }

}
