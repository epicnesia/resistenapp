package id.resisten.app.users;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import id.resisten.app.MainActivity;
import id.resisten.app.R;
import id.resisten.app.retrofit.APIServices;
import id.resisten.app.retrofit.APIUtils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    // Session Manager class
    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_login);

        // Session manager instance
        session = new SessionManager(getApplicationContext());

        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            String extraMessage = extras.getString("SUCCESS_MESSAGE");

            AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
            builder.setMessage(extraMessage)
                    .setNegativeButton("OK", null)
                    .create()
                    .show();
        }

        final EditText etEmail = (EditText) findViewById(R.id.etUserEmail);
        final EditText etPass = (EditText) findViewById(R.id.etUserPass);

        final Button btnLogin = (Button) findViewById(R.id.logoutBtn);

        btnLogin.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    btnLogin.setBackgroundColor(Color.RED);
                    btnLogin.setTextColor(Color.WHITE);
                } else if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    btnLogin.setBackgroundColor(Color.WHITE);
                    btnLogin.setTextColor(Color.RED);
                }
                return false;
            }

        });

        btnLogin.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                final ProgressDialog spinner = new ProgressDialog( LoginActivity.this );
                spinner.setMessage( "Please wait..." );
                spinner.setCancelable( false );
                spinner.show();

                final String password = etPass.getText().toString();
                final String email = etEmail.getText().toString();

                HashMap<String, String> params = new HashMap<>();
                params.put("password", password);
                params.put("email", email);

                APIServices apiServices = APIUtils.getAPIService();
                Call<ResponseBody> result = apiServices.login( params );
                result.enqueue(new Callback<ResponseBody>() {

                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {

                            spinner.dismiss();

                            JSONObject jsonResponse = new JSONObject(response.body().string());
                            boolean status = jsonResponse.getBoolean("status");
                            String message = jsonResponse.getString("message");

                            if(status){

                                // Retrieve JSON data
                                JSONObject loginData    = new JSONObject(message);
                                String id               = loginData.getString("id");
                                String name             = loginData.getString("name");
                                String storeName        = loginData.getString("store_name");
                                String email            = loginData.getString("email");
                                String phone            = loginData.getString("phone");
                                String token            = loginData.getString("api_token");
                                String propict          = loginData.getString("profile_picture");
                                String provinceId       = loginData.getString( "province_id" );
                                String provinceName     = loginData.getString( "province_name" );
                                String cityId           = loginData.getString( "city_id" );
                                String cityName         = loginData.getString( "city_name" );
                                String subdistrictId    = loginData.getString( "subdistrict_id" );
                                String subdistrictName  = loginData.getString( "subdistrict_name" );
                                String address          = loginData.getString( "address" );
                                String type             = loginData.getString("type");

                                // Save user detail to session
                                session.createLoginSession(id, name, storeName, email, phone, token, propict, provinceId, provinceName,
                                        cityId, cityName, subdistrictId, subdistrictName, address, type);

                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();

                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                                builder.setMessage(message)
                                        .setNegativeButton("Retry", null)
                                        .create()
                                        .show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        spinner.dismiss();
                        t.printStackTrace();
                    }

                });
            }

        });

        // Sign Up Link pressed
        final TextView registerLink = (TextView) findViewById(R.id.registerLink);
        registerLink.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){
                Intent loginIntent = new Intent(LoginActivity.this, RegisterActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                LoginActivity.this.startActivity(loginIntent);
                finish();
            }

        });
    }
}
