package id.resisten.app.products;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.HashMap;

import id.resisten.app.R;
import id.resisten.app.orders.OrdersActivity;
import id.resisten.app.retrofit.APIUtils;
import id.resisten.app.users.CircleTransform;
import id.resisten.app.users.LoginActivity;
import id.resisten.app.users.SessionManager;

public class ProductDetailsActivity extends AppCompatActivity {

    // Session Manager class
    private SessionManager session;
    private HashMap<String, String> user;

    private String suppPropict,
            suppName,
            provinceId,
            cityId,
            subdistrictId,
            stock,
            productDate,
            productId,
            productName,
            productPrice,
            productPriceFormatted,
            productCommision,
            productCommisionFormatted,
            productWeight,
            productThumb,
            productDescription;

    private ImageView ivThumbnail,
            ivSuppPropict;

    private TextView tvSuppName,
            tvProductDate,
            tvProductName,
            tvProductPrice,
            tvProductCommision,
            tvProductWeight,
            tvProductStock,
            tvProductDescription;

    private Button btOrder, btCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);

        // Session manager instance
        session = new SessionManager(ProductDetailsActivity.this);
        if(!session.isLoggedIn()){
            Intent loginIntent = new Intent(ProductDetailsActivity.this, LoginActivity.class);
            startActivity(loginIntent);
            finish();
        }

        user = session.getUserDetails();

        getSupportActionBar().setTitle( getResources().getString( R.string.productDetailsTitle ) );
        getSupportActionBar().setDisplayHomeAsUpEnabled( true );

        init_UI();

    }

    private void init_UI() {

        ivThumbnail             = (ImageView) findViewById(R.id.ivDetailsThumbnail);
        ivSuppPropict           = (ImageView) findViewById(R.id.ivDetailsSuppPropict );

        tvSuppName              = (TextView) findViewById(R.id.tvDetailsSuppName );
        tvProductDate           = (TextView) findViewById(R.id.tvDetailsProductDate);
        tvProductName           = (TextView) findViewById(R.id.tvDetailsProductName );
        tvProductPrice          = (TextView) findViewById(R.id.tvDetailsPrice);
        tvProductCommision      = (TextView) findViewById(R.id.tvDetailsCommision);
        tvProductWeight         = (TextView) findViewById(R.id.tvDetailsWeight);
        tvProductStock          = (TextView) findViewById(R.id.tvDetailsStock);
        tvProductDescription    = (TextView) findViewById(R.id.tvDetailsDescription);

        btOrder                 = (Button) findViewById(R.id.btDetailsOrderButton);
        btCall                  = (Button) findViewById(R.id.btDetailsCallButton);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            suppPropict                 = extras.getString( "suppPropict" );
            suppName                    = extras.getString( "suppName" );
            provinceId                  = extras.getString( "provinceID" );
            cityId                      = extras.getString( "cityID" );
            subdistrictId               = extras.getString( "subdistrictID" );
            stock                       = extras.getString( "stock" );
            productDate                 = extras.getString( "productDate" );
            productId                   = extras.getString( "productId" );
            productName                 = extras.getString( "productName" );
            productPrice                = extras.getString( "productPrice" );
            productPriceFormatted       = extras.getString( "productPriceFormatted" );
            productCommision            = extras.getString( "productCommision" );
            productCommisionFormatted   = extras.getString( "productCommisionFormatted" );
            productWeight               = extras.getString( "productWeight" );
            productThumb                = extras.getString( "productThumb" );
            productDescription          = extras.getString( "productDescription" );
        }

        Glide.with( ProductDetailsActivity.this ).load( APIUtils.BASE_URL + "/show_image/" + productThumb )
                .into( ivThumbnail );

        if(!suppPropict.equals("null")){
            Glide.with( ProductDetailsActivity.this ).load( APIUtils.BASE_URL + "/show_image/" + suppPropict )
                    .crossFade()
                    .thumbnail( 0.5f )
                    .bitmapTransform( new CircleTransform( ProductDetailsActivity.this ) )
                    .diskCacheStrategy( DiskCacheStrategy.ALL )
                    .into( ivSuppPropict );
        }

        tvSuppName.setText(suppName);
        tvProductDate.setText(productDate);
        tvProductName.setText(productName);
        tvProductPrice.setText(productPriceFormatted);
        tvProductCommision.setText(productCommisionFormatted);
        tvProductWeight.setText(productWeight + " g");
        tvProductDescription.setText(productDescription);
        tvProductStock.setText( stock );

        btOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent orderIntent = new Intent(ProductDetailsActivity.this, OrdersActivity.class);
                orderIntent.putExtra( "suppPropict", suppPropict );
                orderIntent.putExtra( "suppName", suppName );
                orderIntent.putExtra( "provinceID", provinceId );
                orderIntent.putExtra( "cityID", cityId );
                orderIntent.putExtra( "subdistrictID", subdistrictId );
                orderIntent.putExtra( "stock", stock );
                orderIntent.putExtra( "productDate", productDate );
                orderIntent.putExtra( "productId", productId );
                orderIntent.putExtra( "productName", productName );
                orderIntent.putExtra( "productPrice", productPrice );
                orderIntent.putExtra( "productPriceFormatted", productPriceFormatted );
                orderIntent.putExtra( "productCommision", productCommision );
                orderIntent.putExtra( "productCommisionFormatted", productCommisionFormatted );
                orderIntent.putExtra( "productWeight", productWeight );
                orderIntent.putExtra( "productThumb", productThumb );
                orderIntent.putExtra( "productDescription", productDescription );
                ProductDetailsActivity.this.startActivity( orderIntent );
            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
