package id.resisten.app.products;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

import id.resisten.app.R;
import id.resisten.app.utilities.ScalingUtilities;
import id.resisten.app.users.LoginActivity;
import id.resisten.app.MainActivity;
import id.resisten.app.retrofit.APIServices;
import id.resisten.app.retrofit.APIUtils;
import id.resisten.app.utilities.FileUtils;
import id.resisten.app.users.SessionManager;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

public class UploadProductFragment extends Fragment {

    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;
    private static final int REQUEST_PERMISSION_SETTING = 101;
    private boolean sentToSettings = false;
    private SharedPreferences permissionStatus;

    // Session Manager class
    private SessionManager session;
    private HashMap<String, String> user;

    private ImageView btDec;
    private ImageView btInc;

    private EditText etStock;
    private EditText etProductName;
    private EditText etProductPrice;
    private EditText etProductCommision;
    private EditText etProductWeight;
    private EditText etProductDescription;

    private ImageButton ibPrev;

    private Spinner spPostFor;
    private Button btPost;

    private Bitmap bitmap;

    private int PICK_IMAGE_REQUEST = 1;

    private Uri filePath;

    public UploadProductFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );

        permissionStatus = getActivity().getSharedPreferences("permissionStatus", MODE_PRIVATE);

        // Session manager instance
        session = new SessionManager(getActivity().getApplicationContext());
        if(!session.isLoggedIn()){
            Intent loginIntent = new Intent(this.getActivity(), LoginActivity.class);
            this.getActivity().startActivity(loginIntent);
            getActivity().finish();
        }

        user = session.getUserDetails();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate( R.layout.fragment_upload_product, container, false );

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        btDec               = (ImageView) view.findViewById( R.id.ivDecStock );
        btInc               = (ImageView) view.findViewById( R.id.ivIncStock );

        etStock             = (EditText) view.findViewById( R.id.etProductStock );
        etProductName       = (EditText) view.findViewById( R.id.etProductName );
        etProductPrice      = (EditText) view.findViewById( R.id.etProductPrice );
        etProductCommision  = (EditText) view.findViewById( R.id.etProductCommision );
        etProductWeight     = (EditText) view.findViewById( R.id.etProductWeight );
        etProductDescription= (EditText) view.findViewById( R.id.etProductDescription );

        ibPrev              = (ImageButton) view.findViewById( R.id.ibPreview );

        spPostFor           = (Spinner) view.findViewById( R.id.spPostForChoice );

        btPost              = (Button) view.findViewById( R.id.btPost );

        btDec.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                String valStock = etStock.getText().toString();
                int stock;

                if(valStock.isEmpty()){
                    stock = 0;
                } else {
                    stock = Integer.parseInt( valStock );
                    if(stock > 0){
                        stock--;
                    }
                }
                String txtStock = Integer.toString( stock );
                etStock.setText( txtStock );
            }

        });

        btInc.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                String valStock = etStock.getText().toString();
                int stock;

                if(valStock.isEmpty()){
                    stock = 0;
                } else {
                    stock = Integer.parseInt( valStock );
                }

                stock++;
                String txtStock = Integer.toString( stock );
                etStock.setText( txtStock );
            }

        });

        ibPrev.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                showFileChooser();
            }

        });

        btPost.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                if(filePath != null) {

                    final ProgressDialog spinner = new ProgressDialog( getActivity() );
                    spinner.setMessage( getResources().getString( R.string.pleaseWait ) );
                    spinner.setCancelable( false );
                    spinner.show();

                    // create upload service client
                    APIServices apiServices = APIUtils.getAPIService();

                    // create part for file (photo, video, ...)
                    MultipartBody.Part body = prepareFilePart( "image", filePath );

                    // create a map of data to pass along
                    RequestBody productName         = createPartFromString( etProductName.getText().toString() );
                    RequestBody productStock        = createPartFromString( etStock.getText().toString() );
                    RequestBody productPrice        = createPartFromString( etProductPrice.getText().toString() );
                    RequestBody productCommision    = createPartFromString( etProductCommision.getText().toString() );
                    RequestBody productWeight       = createPartFromString( etProductWeight.getText().toString() );
                    RequestBody productDescritpion  = createPartFromString( etProductDescription.getText().toString() );
                    RequestBody productPostFor      = createPartFromString( String.valueOf(spPostFor.getSelectedItemPosition()+1) );
                    RequestBody apiToken            = createPartFromString( user.get( SessionManager.KEY_TOKEN ) );

                    HashMap<String, RequestBody> params = new HashMap<>();
                    params.put( "name", productName );
                    params.put( "stock", productStock );
                    params.put( "price", productPrice );
                    params.put( "commision", productCommision );
                    params.put( "weight", productWeight );
                    params.put( "description", productDescritpion );
                    params.put( "show_to", productPostFor );
                    params.put( "api_token", apiToken );

                    // finally, execute the request
                    Call<ResponseBody> result = apiServices.postProduct( params, body );
                    result.enqueue( new Callback<ResponseBody>() {

                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            try {

                                JSONObject jsonResponse = new JSONObject( response.body().string() );
                                boolean status = jsonResponse.getBoolean( "status" );
                                String message = jsonResponse.getString( "message" );

                                if (status) {

                                    spinner.dismiss();

                                    Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_LONG).show();

                                    Intent intent = new Intent(getActivity(), MainActivity.class);
                                    intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
                                    getActivity().startActivity( intent );
                                    getActivity().finish();

                                } else {

                                    spinner.dismiss();

                                    AlertDialog.Builder builder = new AlertDialog.Builder( getActivity() );
                                    builder.setMessage( message )
                                            .setNegativeButton( "Retry", null )
                                            .create()
                                            .show();

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            spinner.dismiss();
                            t.printStackTrace();
                        }

                    } );

                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder( getActivity() );
                    builder.setMessage("Please select an image first..!")
                            .setNegativeButton( "Retry", null )
                            .create()
                            .show();
                }
            }

        });

    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    //Show Information about why you need the permission
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Need Storage Permission");
                    builder.setMessage("This app needs storage permission.");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                } else if (permissionStatus.getBoolean(Manifest.permission.READ_EXTERNAL_STORAGE,false)) {
                    //Previously Permission Request was cancelled with 'Dont Ask Again',
                    // Redirect to Settings after showing Information about why you need the permission
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Need Storage Permission");
                    builder.setMessage("This app needs storage permission.");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            sentToSettings = true;
                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                            intent.setData(uri);
                            startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                            Toast.makeText(getActivity().getBaseContext(), "Go to Permissions to Grant Storage", Toast.LENGTH_LONG).show();
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                } else {
                    //just request the permission
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);
                }


                SharedPreferences.Editor editor = permissionStatus.edit();
                editor.putBoolean(Manifest.permission.READ_EXTERNAL_STORAGE,true);
                editor.commit();

            }

            filePath = data.getData();
            try {
                //Getting the Bitmap from Gallery
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath);
                //Setting the Bitmap to ImageView
                ibPrev.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 120, 120, false));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private RequestBody createPartFromString(String str) {
        return RequestBody.create( MediaType.parse("text/plain"), str );
    }

    private MultipartBody.Part prepareFilePart(String partName, Uri fileUri) {

        Uri newUri = Uri.parse(decodeFile(fileUri.toString(), 1600, 900));

        // use the FileUtils to get the actual file by uri
        File file = FileUtils.getFile( getActivity().getApplicationContext(), newUri );

        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse( getActivity().getContentResolver().getType( newUri ) ),
                        file
                );

        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData( partName, file.getName(), requestFile );

    }

    private String decodeFile(String path, int DESIREDWIDTH, int DESIREDHEIGHT) {
        String strMyImagePath = null;
        Bitmap scaledBitmap = null;

        try {
            // Part 1: Decode image
            Bitmap unscaledBitmap = ScalingUtilities.decodeFile(path, DESIREDWIDTH, DESIREDHEIGHT, ScalingUtilities.ScalingLogic.FIT);

            if (!(unscaledBitmap.getWidth() <= DESIREDWIDTH && unscaledBitmap.getHeight() <= DESIREDHEIGHT)) {
                // Part 2: Scale image
                scaledBitmap = ScalingUtilities.createScaledBitmap(unscaledBitmap, DESIREDWIDTH, DESIREDHEIGHT, ScalingUtilities.ScalingLogic.FIT);
            } else {
                unscaledBitmap.recycle();
                return path;
            }

            // Store to tmp file

            String extr = Environment.getExternalStorageDirectory().toString();
            File mFolder = new File(extr + "/TMMFOLDER");
            if (!mFolder.exists()) {
                mFolder.mkdir();
            }

            String s = "tmp.png";

            File f = new File(mFolder.getAbsolutePath(), s);

            strMyImagePath = f.getAbsolutePath();
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(f);
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 75, fos);
                fos.flush();
                fos.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            scaledBitmap.recycle();
        } catch (Throwable e) {
        }

        if (strMyImagePath == null) {
            return path;
        }
        return strMyImagePath;

    }

}
