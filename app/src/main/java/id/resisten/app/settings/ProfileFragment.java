package id.resisten.app.settings;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import id.resisten.app.MainActivity;
import id.resisten.app.R;
import id.resisten.app.retrofit.APIServices;
import id.resisten.app.retrofit.APIUtils;
import id.resisten.app.utilities.FileUtils;
import id.resisten.app.retrofit.ROAPIServices;
import id.resisten.app.retrofit.ROAPIUtils;
import id.resisten.app.utilities.ScalingUtilities;
import id.resisten.app.spinner.CityDataModel;
import id.resisten.app.spinner.ProvinceDataModel;
import id.resisten.app.spinner.SubdistrictDataModel;
import id.resisten.app.users.LoginActivity;
import id.resisten.app.users.SessionManager;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

public class ProfileFragment extends Fragment {

    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;
    private static final int REQUEST_PERMISSION_SETTING = 101;
    private boolean sentToSettings = false;
    private SharedPreferences permissionStatus;

    // Session Manager class
    private SessionManager session;
    private HashMap<String, String> user;

    private ProgressDialog spinner;

    private ImageView ivProfilePicture;

    private String uPropict,
            uName,
            uEmail,
            uPhone,
            uStoreName,
            uAddress,
            uProvinceId,
            uProvinceName,
            uCityId,
            uCityName,
            uSubdistrictId,
            uSubdistrictName;

    private EditText etProfileName,
            etProfileEmail,
            etProfilePhone,
            etProfileStoreName,
            etProfileOldPassword,
            etProfileNewPassword,
            etProfileAddress,
            etTargetSales,
            etTargetTurnover;

    private Spinner spProfileProvince,
            spProfileCity,
            spProfileSubdistrict;

    private Button btProfilePost;

    private List<ProvinceDataModel> provinceList;
    private ArrayAdapter<ProvinceDataModel> provinceAdapter;

    private List<CityDataModel> cityList;
    private ArrayAdapter<CityDataModel> cityAdapter;

    private List<SubdistrictDataModel> subdistrictList;
    private ArrayAdapter<SubdistrictDataModel> subdistrictAdapter;

    private int PICK_IMAGE_REQUEST = 1;

    private Bitmap bitmap;

    private Uri filePath;

    private MultipartBody.Part body;

    public ProfileFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );

        permissionStatus = getActivity().getSharedPreferences("permissionStatus", MODE_PRIVATE);

        // Session manager instance
        session = new SessionManager(getActivity().getApplicationContext());
        if(!session.isLoggedIn()){
            Intent loginIntent = new Intent(this.getActivity(), LoginActivity.class);
            this.getActivity().startActivity(loginIntent);
            getActivity().finish();
        }

        user = session.getUserDetails();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate( R.layout.fragment_setting_profile, container, false );
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        spinner = new ProgressDialog( getActivity() );

        ivProfilePicture    = (ImageView) view.findViewById( R.id.ivProfilePicture );

        etProfileName       = (EditText) view.findViewById( R.id.etProfileName );
        etProfileEmail      = (EditText) view.findViewById( R.id.etProfileEmail );
        etProfilePhone      = (EditText) view.findViewById( R.id.etProfilePhone );
        etProfileStoreName  = (EditText) view.findViewById( R.id.etProfileStoreName );
        etProfileOldPassword= (EditText) view.findViewById( R.id.etProfileOldPass );
        etProfileNewPassword= (EditText) view.findViewById( R.id.etProfileNewPass );
        etProfileAddress    = (EditText) view.findViewById( R.id.etProfileAddress );
        etTargetSales       = (EditText) view.findViewById( R.id.etTargetSales );
        etTargetTurnover    = (EditText) view.findViewById( R.id.etTargetTurnover );

        spProfileProvince   = (Spinner) view.findViewById( R.id.spProfileProvince );
        spProfileCity       = (Spinner) view.findViewById( R.id.spProfileCity );
        spProfileSubdistrict= (Spinner) view.findViewById( R.id.spProfileSubdistrict );

        btProfilePost       = (Button) view.findViewById( R.id.btProfilePost );

        uPropict            = user.get( SessionManager.KEY_PROPICT );
        uName               = user.get( SessionManager.KEY_NAME );
        uEmail              = user.get( SessionManager.KEY_EMAIL );
        uPhone              = user.get( SessionManager.KEY_PHONE );
        uStoreName          = user.get( SessionManager.KEY_STORE_NAME );
        uAddress            = user.get( SessionManager.KEY_ADDRESS );
        uProvinceId         = user.get( SessionManager.KEY_PROVINCE_ID );
        uProvinceName       = user.get( SessionManager.KEY_PROVINCE_NAME );
        uCityId             = user.get( SessionManager.KEY_CITY_ID );
        uCityName           = user.get( SessionManager.KEY_CITY_NAME );
        uSubdistrictId      = user.get( SessionManager.KEY_SUBDISTRICT_ID );
        uSubdistrictName    = user.get( SessionManager.KEY_SUBDISTRICT_NAME );

        // Show progress dialog
        spinner.setMessage( getResources().getString( R.string.pleaseWait ) );
        spinner.show();

        // Initializing profile data
        init_profile_data();

        // Initializing spinners data
        setup_province_spinner();

        // Initializing target data for current month
        load_target();

        // Showing image chooser when profile picture clicked
        ivProfilePicture.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        } );

        // Update data when post button clicked
        btProfilePost.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                spinner.setMessage( getResources().getString( R.string.pleaseWait ) );
                spinner.setCancelable( false );
                spinner.show();

                // create upload service client
                APIServices apiServices = APIUtils.getAPIService();

                // create part for file if image changed
                if(filePath != null){
                    body = prepareFilePart( "profile_picture", filePath );
                }

                // create a map of data to pass along
                RequestBody apiToken        = createPartFromString( user.get( SessionManager.KEY_TOKEN ) );
                RequestBody email           = createPartFromString( etProfileEmail.getText().toString() );
                RequestBody name            = createPartFromString( etProfileName.getText().toString() );
                RequestBody phone           = createPartFromString( etProfilePhone.getText().toString() );
                RequestBody storeName       = createPartFromString( etProfileStoreName.getText().toString() );
                RequestBody oldPassword     = createPartFromString( etProfileOldPassword.getText().toString() );
                RequestBody newPassword     = createPartFromString( etProfileNewPassword.getText().toString() );
                RequestBody provinceId      = createPartFromString( String.valueOf( provinceList.get( spProfileProvince.getSelectedItemPosition() ).getId() ) );
                RequestBody provinceName    = createPartFromString( provinceList.get( spProfileProvince.getSelectedItemPosition() ).getProvince() );
                RequestBody cityId          = createPartFromString( String.valueOf( cityList.get( spProfileCity.getSelectedItemPosition() ).getId() ) );
                RequestBody cityName        = createPartFromString( cityList.get( spProfileCity.getSelectedItemPosition() ).getType() + " " + cityList.get( spProfileCity.getSelectedItemPosition() ).getCityName() );
                RequestBody subdistrictId   = createPartFromString( String.valueOf( subdistrictList.get( spProfileSubdistrict.getSelectedItemPosition() ).getId() ) );
                RequestBody subdistrictName = createPartFromString( subdistrictList.get( spProfileSubdistrict.getSelectedItemPosition() ).getSubdistrictName() );
                RequestBody address         = createPartFromString( etProfileAddress.getText().toString() );

                RequestBody sales           = createPartFromString( etTargetSales.getText().toString() );
                RequestBody turnover        = createPartFromString( etTargetTurnover.getText().toString() );

                HashMap<String, RequestBody> params = new HashMap<>();
                params.put( "api_token", apiToken );
                params.put( "email", email );
                params.put( "name", name );
                params.put( "phone", phone );
                params.put( "store_name", storeName );
                params.put( "old_password", oldPassword );
                params.put( "new_password", newPassword );
                params.put( "province_id", provinceId );
                params.put( "province_name", provinceName );
                params.put( "city_id", cityId );
                params.put( "city_name", cityName );
                params.put( "subdistrict_id", subdistrictId );
                params.put( "subdistrict_name", subdistrictName );
                params.put( "address", address );
                params.put( "sales", sales );
                params.put( "turnover", turnover );

                // finally, execute the request
                Call<ResponseBody> result = apiServices.updateProfile( params, body );
                result.enqueue( new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        try {

                            spinner.dismiss();

                            JSONObject jsonResponse = new JSONObject( response.body().string() );
                            boolean status = jsonResponse.getBoolean( "status" );
                            String message = jsonResponse.getString( "message" );
                            String propict = jsonResponse.getString( "profile_picture" );

                            if (status) {

                                Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                                // Update session
                                session.createLoginSession(
                                        user.get( SessionManager.KEY_ID ),
                                        etProfileName.getText().toString(),
                                        etProfileStoreName.getText().toString(),
                                        etProfileEmail.getText().toString(),
                                        etProfilePhone.getText().toString(),
                                        user.get( SessionManager.KEY_TOKEN ),
                                        propict,
                                        String.valueOf( provinceList.get( spProfileProvince.getSelectedItemPosition() ).getId() ),
                                        provinceList.get( spProfileProvince.getSelectedItemPosition() ).getProvince(),
                                        String.valueOf( cityList.get( spProfileCity.getSelectedItemPosition() ).getId() ),
                                        cityList.get( spProfileCity.getSelectedItemPosition() ).getType() + " " + cityList.get( spProfileCity.getSelectedItemPosition() ).getCityName(),
                                        String.valueOf( subdistrictList.get( spProfileSubdistrict.getSelectedItemPosition() ).getId() ),
                                        subdistrictList.get( spProfileSubdistrict.getSelectedItemPosition() ).getSubdistrictName(),
                                        etProfileAddress.getText().toString(),
                                        user.get( SessionManager.KEY_TYPE )
                                );

                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder( getActivity() );
                                builder.setMessage( message )
                                        .setNegativeButton( "Retry", null )
                                        .create()
                                        .show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        spinner.dismiss();
                        t.printStackTrace();
                    }
                } );

            }
        } );

    }

    private void init_profile_data() {

        if(!uPropict.equals( "null" )){
            Glide.with( getActivity() ).load( APIUtils.BASE_URL + "/show_image/" + uPropict )
                    .into( ivProfilePicture );
        }

        etProfileName.setText( uName );
        etProfileEmail.setText( uEmail );
        etProfilePhone.setText( uPhone );

        if(!uStoreName.equals( "null" )){
            etProfileStoreName.setText( uStoreName );
        }

        if(!uAddress.equals( "null" )){
            etProfileAddress.setText( uAddress );
        }

    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    //Show Information about why you need the permission
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Need Storage Permission");
                    builder.setMessage("This app needs storage permission.");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                } else if (permissionStatus.getBoolean(Manifest.permission.READ_EXTERNAL_STORAGE,false)) {
                    //Previously Permission Request was cancelled with 'Dont Ask Again',
                    // Redirect to Settings after showing Information about why you need the permission
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Need Storage Permission");
                    builder.setMessage("This app needs storage permission.");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            sentToSettings = true;
                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                            intent.setData(uri);
                            startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                            Toast.makeText(getActivity().getBaseContext(), "Go to Permissions to Grant Storage", Toast.LENGTH_LONG).show();
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                } else {
                    //just request the permission
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);
                }


                SharedPreferences.Editor editor = permissionStatus.edit();
                editor.putBoolean(Manifest.permission.READ_EXTERNAL_STORAGE,true);
                editor.commit();

            }

            filePath = data.getData();
            try {
                //Getting the Bitmap from Gallery
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath);
                //Setting the Bitmap to ImageView
                ivProfilePicture.setImageBitmap( Bitmap.createScaledBitmap(bitmap, 80, 80, false));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private RequestBody createPartFromString(String str) {
        return RequestBody.create( MediaType.parse("text/plain"), str );
    }

    private MultipartBody.Part prepareFilePart(String partName, Uri fileUri) {

        Uri newUri = Uri.parse(decodeFile(fileUri.toString(), 1600, 900));

        // use the FileUtils to get the actual file by uri
        File file = FileUtils.getFile( getActivity().getApplicationContext(), newUri );

        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse( getActivity().getContentResolver().getType( newUri ) ),
                        file
                );

        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData( partName, file.getName(), requestFile );

    }

    private String decodeFile(String path, int DESIREDWIDTH, int DESIREDHEIGHT) {
        String strMyImagePath = null;
        Bitmap scaledBitmap = null;

        try {
            // Part 1: Decode image
            Bitmap unscaledBitmap = ScalingUtilities.decodeFile(path, DESIREDWIDTH, DESIREDHEIGHT, ScalingUtilities.ScalingLogic.FIT);

            if (!(unscaledBitmap.getWidth() <= DESIREDWIDTH && unscaledBitmap.getHeight() <= DESIREDHEIGHT)) {
                // Part 2: Scale image
                scaledBitmap = ScalingUtilities.createScaledBitmap(unscaledBitmap, DESIREDWIDTH, DESIREDHEIGHT, ScalingUtilities.ScalingLogic.FIT);
            } else {
                unscaledBitmap.recycle();
                return path;
            }

            // Store to tmp file

            String extr = Environment.getExternalStorageDirectory().toString();
            File mFolder = new File(extr + "/TMMFOLDER");
            if (!mFolder.exists()) {
                mFolder.mkdir();
            }

            String s = "tmp.png";

            File f = new File(mFolder.getAbsolutePath(), s);

            strMyImagePath = f.getAbsolutePath();
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(f);
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 75, fos);
                fos.flush();
                fos.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            scaledBitmap.recycle();
        } catch (Throwable e) {
        }

        if (strMyImagePath == null) {
            return path;
        }
        return strMyImagePath;

    }

    /*
     * Setup Province Spinner
     */
    private void setup_province_spinner() {

        provinceList    = new ArrayList<>();
        provinceList.add( new ProvinceDataModel( 0, getResources().getString( R.string.provinceSpinnerHint ) ) );

        ROAPIServices roApiServices = ROAPIUtils.getROAPIService();
        Call<ResponseBody> result   = roApiServices.getProvinces();
        result.enqueue( new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {

                    JSONObject jsonResponse = new JSONObject( response.body().string() );
                    JSONObject rajaongkir   = jsonResponse.getJSONObject( "rajaongkir" );
                    JSONObject status       = rajaongkir.getJSONObject( "status" );
                    int statusCode          = status.getInt( "code" );

                    if(statusCode == 200){

                        JSONArray arrayResults = rajaongkir.getJSONArray( "results" );
                        if(arrayResults.length() > 0){
                            for(int i = 0; i < arrayResults.length(); i++){

                                JSONObject object = arrayResults.getJSONObject( i );
                                ProvinceDataModel provinceData = new ProvinceDataModel(
                                        object.getInt( "province_id" ),
                                        object.getString( "province" )
                                );

                                provinceList.add( provinceData );

                            }
                        }

                        provinceAdapter = new ArrayAdapter<>( getActivity(), android.R.layout.simple_spinner_item, provinceList );
                        provinceAdapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );

                        spProfileProvince.setAdapter( provinceAdapter );
                        if(!uProvinceName.equals( "null" )){
                            spProfileProvince.setSelection( getIndex( spProfileProvince, uProvinceName ) );
                        }
                        if(!uProvinceId.equals( "null" )){
                            setup_city_spinner( Integer.parseInt( uProvinceId ) );
                        }
                        if(!uCityId.equals( "null" )) {
                            setup_subdistrict_spinner( Integer.parseInt( uCityId ) );
                        }

                    } else {
                        String message = status.getString( "decription" );

                        AlertDialog.Builder builder = new AlertDialog.Builder( getActivity() );
                        builder.setMessage( message )
                                .setNegativeButton( "Retry", null )
                                .create()
                                .show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
            }
        } );

        spProfileProvince.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position > 0){
                    setup_city_spinner(provinceList.get( position ).getId());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        } );

    }

    /*
     * Setup City Spinner
     */
    private void setup_city_spinner(final int provinceId) {

        cityList    = new ArrayList<>();
        cityList.add( new CityDataModel( 0, 0, "", getResources().getString( R.string.citySpinnerHint1 ), getResources().getString( R.string.citySpinnerHint2 ), "" ) );

        ROAPIServices roApiServices = ROAPIUtils.getROAPIService();
        Call<ResponseBody> result   = roApiServices.getCities(provinceId);
        result.enqueue( new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {

                    JSONObject jsonResponse = new JSONObject( response.body().string() );
                    JSONObject rajaongkir   = jsonResponse.getJSONObject( "rajaongkir" );
                    JSONObject status       = rajaongkir.getJSONObject( "status" );
                    int statusCode          = status.getInt( "code" );

                    if(statusCode == 200){

                        JSONArray arrayResults = rajaongkir.getJSONArray( "results" );
                        if(arrayResults.length() > 0){
                            for(int i = 0; i < arrayResults.length(); i++){

                                JSONObject object = arrayResults.getJSONObject( i );
                                CityDataModel cityData = new CityDataModel(
                                        object.getInt( "city_id" ),
                                        object.getInt( "province_id" ),
                                        object.getString( "province" ),
                                        object.getString( "type" ),
                                        object.getString( "city_name" ),
                                        object.getString( "postal_code" )
                                );

                                cityList.add( cityData );

                            }
                        }

                        cityAdapter = new ArrayAdapter<>( getActivity(), android.R.layout.simple_spinner_item, cityList );
                        cityAdapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );

                        spProfileCity.setAdapter( cityAdapter );
                        if(!uCityName.equals( "null" )){
                            spProfileCity.setSelection( getIndex( spProfileCity, uCityName ) );
                        }

                    } else {
                        String message = status.getString( "decription" );

                        AlertDialog.Builder builder = new AlertDialog.Builder( getActivity() );
                        builder.setMessage( message )
                                .setNegativeButton( "Retry", null )
                                .create()
                                .show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
            }
        } );

        spProfileCity.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setup_subdistrict_spinner(cityList.get( position ).getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        } );

    }

    /*
     * Setup Subdistrict Spinner
     */
    private void setup_subdistrict_spinner(final int cityId) {

        subdistrictList    = new ArrayList<>();
        subdistrictList.add( new SubdistrictDataModel( 0, 0, "", 0, "", "", getResources().getString( R.string.subdistrictSpinnerHint ) ) );

        ROAPIServices roApiServices = ROAPIUtils.getROAPIService();
        Call<ResponseBody> result   = roApiServices.getSubdistrict(cityId);
        result.enqueue( new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {

                    JSONObject jsonResponse = new JSONObject( response.body().string() );
                    JSONObject rajaongkir   = jsonResponse.getJSONObject( "rajaongkir" );
                    JSONObject status       = rajaongkir.getJSONObject( "status" );
                    int statusCode          = status.getInt( "code" );

                    if(statusCode == 200){

                        JSONArray arrayResults = rajaongkir.getJSONArray( "results" );
                        if(arrayResults.length() > 0){
                            for(int i = 0; i < arrayResults.length(); i++){

                                JSONObject object = arrayResults.getJSONObject( i );
                                SubdistrictDataModel subdistrictData = new SubdistrictDataModel(
                                        object.getInt( "subdistrict_id" ),
                                        object.getInt( "province_id" ),
                                        object.getString( "province" ),
                                        object.getInt( "city_id" ),
                                        object.getString( "city" ),
                                        object.getString( "type" ),
                                        object.getString( "subdistrict_name" )
                                );

                                subdistrictList.add( subdistrictData );

                            }
                        }

                        subdistrictAdapter = new ArrayAdapter<>( getActivity(), android.R.layout.simple_spinner_item, subdistrictList );
                        subdistrictAdapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );

                        spProfileSubdistrict.setAdapter( subdistrictAdapter );
                        if(!uSubdistrictName.equals( "null" )){
                            spProfileSubdistrict.setSelection( getIndex( spProfileSubdistrict, uSubdistrictName ) );
                        }

                    } else {
                        String message = status.getString( "decription" );

                        AlertDialog.Builder builder = new AlertDialog.Builder( getActivity() );
                        builder.setMessage( message )
                                .setNegativeButton( "Retry", null )
                                .create()
                                .show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
            }
        } );

    }

    /*
     * Get index for spinner's data
     */
    private int getIndex(Spinner spinner, String myString)
    {
        int index = 0;

        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)){
                index = i;
                break;
            }
        }
        return index;
    }

    /*
     * Load target data
     */
    private void load_target() {

        APIServices apiServices = APIUtils.getAPIService();
        Call<ResponseBody> result = apiServices.loadTarget( user.get( SessionManager.KEY_TOKEN ) );
        result.enqueue( new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {

                    spinner.dismiss();

                    JSONObject jsonResponse = new JSONObject( response.body().string() );
                    boolean status = jsonResponse.getBoolean( "status" );
                    String message = jsonResponse.getString( "message" );

                    if(status){

                        if(!message.equals( "null" )){
                            JSONObject targetData = new JSONObject( message );
                            etTargetSales.setText( targetData.getString( "sales" ) );
                            etTargetTurnover.setText( targetData.getString( "turnover" ) );
                        }

                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage(message)
                                .setNegativeButton("Retry", null)
                                .create()
                                .show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
            }
        } );

    }

}
