package id.resisten.app.notifications;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import id.resisten.app.R;
import id.resisten.app.retrofit.APIServices;
import id.resisten.app.retrofit.APIUtils;
import id.resisten.app.users.LoginActivity;
import id.resisten.app.users.SessionManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class CoopRequestFragment extends Fragment {

    // Session Manager class
    private SessionManager session;
    private HashMap<String, String> user;

    private int page;

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private CoopRequestAdapter coopRequestAdapter;
    private List<CoopRequestModel> coopRequestList;

    public CoopRequestFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Session manager instance
        session = new SessionManager(getActivity().getApplicationContext());
        if(!session.isLoggedIn()){
            Intent loginIntent = new Intent(this.getActivity(), LoginActivity.class);
            this.getActivity().startActivity(loginIntent);
            getActivity().finish();
        }

        user = session.getUserDetails();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_coop_request, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        page = 1;

        recyclerView = (RecyclerView) view.findViewById( R.id.rvCoopRequest );
        recyclerView.setHasFixedSize(true);
        coopRequestList = new ArrayList<>();

        load_data(page);

        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);

        coopRequestAdapter = new CoopRequestAdapter(getActivity(), coopRequestList);
        recyclerView.setAdapter(coopRequestAdapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if(linearLayoutManager.findLastCompletelyVisibleItemPosition() == coopRequestList.size()-1){
                    load_data(++page);
                }
            }
        });

    }

    private void load_data(final int page) {

        AsyncTask<Integer, Void, Void> task = new AsyncTask<Integer, Void, Void>() {
            @Override
            protected Void doInBackground(Integer... params) {

                APIServices apiServices = APIUtils.getAPIService();
                Call<ResponseBody> result = apiServices.getCoopRequest( user.get( SessionManager.KEY_TOKEN), page );
                try {

                    Response<ResponseBody> response = result.execute();

                    JSONObject jsonResponse = new JSONObject( response.body().string() );
                    boolean status = jsonResponse.getBoolean( "status" );

                    if(status){

                        JSONArray array = jsonResponse.getJSONArray( "message" );
                        for(int i=0;i<array.length();i++){

                            JSONObject object = array.getJSONObject( i );

                            CoopRequestModel data = new CoopRequestModel(
                                    object.getInt( "id" ),
                                    object.getString( "name" ),
                                    object.getString( "email" ),
                                    object.getString( "phone" ),
                                    object.getString( "profile_picture" )
                            );

                            coopRequestList.add( data );

                        }

                    } else {

                        String message = jsonResponse.getString( "message" );

                        AlertDialog.Builder builder = new AlertDialog.Builder( getActivity() );
                        builder.setMessage( message )
                                .setNegativeButton( "Retry", null )
                                .create()
                                .show();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid){
                coopRequestAdapter.notifyDataSetChanged();
            }
        };

        task.execute();

    }

}
