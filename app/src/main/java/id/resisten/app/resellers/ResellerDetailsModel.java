package id.resisten.app.resellers;

/**
 * Created by LEKTOP on 19-May-17.
 */

public class ResellerDetailsModel {

    private int id, grandTotal;
    private String date, supplierName, productName, status;

    public ResellerDetailsModel(int id, int grandTotal, String date, String supplierName, String productName, String status) {
        this.id = id;
        this.grandTotal = grandTotal;
        this.date = date;
        this.supplierName = supplierName;
        this.productName = productName;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(int grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
