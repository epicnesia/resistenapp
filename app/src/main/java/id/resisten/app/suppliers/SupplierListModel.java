package id.resisten.app.suppliers;

/**
 * Created by LEKTOP on 14-Apr-17.
 */

public class SupplierListModel {

    private int id;
    private String name, storeName, image;

    public SupplierListModel(int id, String name, String storeName, String image) {
        this.id = id;
        this.name = name;
        this.storeName = storeName;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
