package id.resisten.app.orders;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import id.resisten.app.MainActivity;
import id.resisten.app.R;
import id.resisten.app.retrofit.APIServices;
import id.resisten.app.retrofit.APIUtils;
import id.resisten.app.settings.BanksModel;
import id.resisten.app.users.LoginActivity;
import id.resisten.app.users.SessionManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class OrderSuccessActivity extends AppCompatActivity {

    // Session Manager class
    private SessionManager session;
    private HashMap<String, String> user;

    private String orderId, sellerId, totalPayment;

    private TextView tvOrderId, tvTotalPayment;

    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private OrderSuccessAdapter orderSuccessAdapter;
    private List<BanksModel> banksList;

    private int page;

    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_order_success );

        // Session manager instance
        session = new SessionManager(OrderSuccessActivity.this);
        if(!session.isLoggedIn()){
            Intent loginIntent = new Intent(OrderSuccessActivity.this, LoginActivity.class);
            startActivity(loginIntent);
            finish();
        }

        user = session.getUserDetails();

        getSupportActionBar().setTitle( getResources().getString( R.string.orderSuccess ) );
        getSupportActionBar().setDisplayHomeAsUpEnabled( true );

        tvOrderId       = (TextView) findViewById( R.id.tvOrderSuccessOrderID );
        tvTotalPayment  = (TextView) findViewById( R.id.tvOrderSuccessTotalPayment );

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            orderId     = extras.getString( "order_id" );
            sellerId    = extras.getString( "seller_id" );
            totalPayment= extras.getString( "total_payment" );
        }

        tvOrderId.setText( orderId );
        tvTotalPayment.setText( totalPayment );

        progressBar     = (ProgressBar) findViewById(R.id.pbOrderSuccessBanks);

        page = 1;

        recyclerView    = (RecyclerView) findViewById(R.id.rvOrderSuccessBanks);
        recyclerView.setHasFixedSize( true );
        banksList       = new ArrayList<>();

        load_data(page);

        layoutManager   = new LinearLayoutManager( OrderSuccessActivity.this );
        recyclerView.setLayoutManager( layoutManager );

        orderSuccessAdapter = new OrderSuccessAdapter(OrderSuccessActivity.this, banksList);
        recyclerView.setAdapter(orderSuccessAdapter);

        recyclerView.addOnScrollListener( new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if(layoutManager.findLastCompletelyVisibleItemPosition() == banksList.size()-1){
                    load_data( ++page );
                }
            }
        } );

    }

    private void load_data(final int page) {

        AsyncTask<Integer, Void, Void> task = new AsyncTask<Integer, Void, Void>() {
            @Override
            protected Void doInBackground(Integer... params) {

                APIServices apiServices = APIUtils.getAPIService();
                Call<ResponseBody> result = apiServices.getSellerBanks( user.get( SessionManager.KEY_TOKEN), page, Integer.parseInt(sellerId) );
                try {

                    Response<ResponseBody> response = result.execute();

                    JSONObject jsonResponse = new JSONObject( response.body().string() );
                    boolean status = jsonResponse.getBoolean( "status" );

                    if(status){

                        JSONObject message = jsonResponse.getJSONObject( "message" );

                        JSONArray array = message.getJSONArray( "data" );
                        for(int i=0;i<array.length();i++){

                            JSONObject object = array.getJSONObject( i );

                            BanksModel data = new BanksModel (
                                    object.getInt( "id" ),
                                    object.getString( "bank_name" ),
                                    object.getString( "account_number" ),
                                    object.getString( "account_name" )
                            );

                            banksList.add( data );

                        }

                    } else {

                        String message = jsonResponse.getString( "message" );

                        AlertDialog.Builder builder = new AlertDialog.Builder( OrderSuccessActivity.this );
                        builder.setMessage( message )
                                .setNegativeButton( "Retry", null )
                                .create()
                                .show();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid){
                progressBar.setVisibility( View.GONE );
                orderSuccessAdapter.notifyDataSetChanged();
            }
        };

        task.execute();

    }

    @Override
    public void onBackPressed() {
        Intent mainIntent = new Intent(OrderSuccessActivity.this, MainActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(mainIntent);
        finish();

        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent mainIntent = new Intent(OrderSuccessActivity.this, MainActivity.class);
                mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(mainIntent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
