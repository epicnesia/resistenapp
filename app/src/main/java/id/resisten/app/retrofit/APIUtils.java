package id.resisten.app.retrofit;

/**
 * Created by LEKTOP on 04-Apr-17.
 */

public class APIUtils {

    private APIUtils() {}

    public static final String BASE_URL = "https://apps.resisten.id/";

    public static APIServices getAPIService() {

        return RetrofitClient.getClient(BASE_URL).create(APIServices.class);
    }
}
