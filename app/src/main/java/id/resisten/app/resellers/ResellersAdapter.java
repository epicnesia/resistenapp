package id.resisten.app.resellers;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import id.resisten.app.R;
import id.resisten.app.retrofit.APIUtils;
import id.resisten.app.suppliers.SuppliersAdapter;
import id.resisten.app.users.CircleTransform;

/**
 * Created by LEKTOP on 18-May-17.
 */

public class ResellersAdapter extends RecyclerView.Adapter<ResellersAdapter.ViewHolder> {

    private Context context;
    private List<ResellersModel> resellersList;

    public ResellersAdapter(Context context, List<ResellersModel> resellersList) {
        this.context = context;
        this.resellersList = resellersList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from( parent.getContext() ).inflate( R.layout.fragment_reseller_rv, parent, false );
        return new ViewHolder( itemView );
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        if(!resellersList.get( position ).getImage().equals( "null" )) {
            Glide.with( context ).load( APIUtils.BASE_URL + "/show_image/" + resellersList.get( position ).getImage() )
                    .crossFade()
                    .thumbnail( 0.5f )
                    .bitmapTransform( new CircleTransform( context ) )
                    .diskCacheStrategy( DiskCacheStrategy.ALL )
                    .into( holder.propict );
        }

        holder.userName.setText( resellersList.get( position ).getName() );

        if( resellersList.get( position ).getRelationStatus() == 2 ){
            holder.actionButton.setBackgroundResource( R.color.colorPrimary );
            holder.actionButton.setText( R.string.chat );
            holder.actionButton.setTextColor( Color.WHITE );
        }

        holder.rlReseller.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent( context, ResellerDetailsActivity.class );
                intent.putExtra( "reseller_id", resellersList.get( position ).getId() + "" );
                intent.putExtra( "reseller_name", resellersList.get( position ).getName() );
                intent.putExtra( "reseller_phone", resellersList.get( position ).getPhone() );
                intent.putExtra( "reseller_email", resellersList.get( position ).getEmail() );
                intent.putExtra( "reseller_propict", resellersList.get( position ).getImage() );
                context.startActivity( intent );

            }
        } );
    }

    @Override
    public int getItemCount() {
        return resellersList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public RelativeLayout rlReseller;
        public ImageView propict;
        public TextView userName;
        public Button actionButton;

        public ViewHolder(View itemView) {
            super( itemView );

            rlReseller  = (RelativeLayout) itemView.findViewById( R.id.rlReseller );
            propict     = (ImageView) itemView.findViewById( R.id.ivResellerPropict );
            userName    = (TextView) itemView.findViewById( R.id.tvResellerName );
            actionButton= (Button) itemView.findViewById( R.id.btActionReseller );
        }
    }
}
