package id.resisten.app;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.HashMap;

import id.resisten.app.retrofit.APIUtils;
import id.resisten.app.users.LoginActivity;
import id.resisten.app.payments.ConfirmPaymentFragment;
import id.resisten.app.fragments.CustomerFragment;
import id.resisten.app.fragments.FavoriteFragment;
import id.resisten.app.fragments.MessagesFragment;
import id.resisten.app.products.NewProductFragment;
import id.resisten.app.fragments.NotesFragment;
import id.resisten.app.notifications.NotificationsFragment;
import id.resisten.app.orders.OrderFragment;
import id.resisten.app.resellers.ResellerFragment;
import id.resisten.app.settings.SettingsFragment;
import id.resisten.app.shipments.ShipmentFragment;
import id.resisten.app.suppliers.SupplierFragment;
import id.resisten.app.fragments.TargetFragment;
import id.resisten.app.products.UploadProductFragment;
import id.resisten.app.users.CircleTransform;
import id.resisten.app.users.SessionManager;

public class MainActivity extends AppCompatActivity {

    // Session Manager class
    private SessionManager session;
    private HashMap<String, String> user;
    private int userType;

    private NavigationView navigationView;
    private DrawerLayout drawer;
    private View navHeader;
    private ImageView  imgProfile;
    private TextView txtName, txtEmail;
    private Toolbar toolbar;

    // index to identify current nav menu item
    public static int navItemIndex = 0;

    // tags used to attach the fragments
    private static final String TAG_NEW_PRODUCT = "newProduct";
    private static final String TAG_MESSAGES = "messages";
    private static final String TAG_ORDER = "order";
    private static final String TAG_TARGET = "target";
    private static final String TAG_NOTIFICATIONS = "notifications";
    private static final String TAG_FAVORITE = "favorite";
    private static final String TAG_UPLOAD_PRODUCT = "uploadProduct";
    private static final String TAG_SUPPLIER = "supplier";
    private static final String TAG_RESELLER = "reseller";
    private static final String TAG_CUSTOMER = "customer";
    private static final String TAG_CONFIRM_PAYMENT = "confirmPayment";
    private static final String TAG_SHIPMENT = "shipment";
    private static final String TAG_NOTES = "notes";
    private static final String TAG_SETTINGS = "settings";
    public static String CURRENT_TAG = TAG_NEW_PRODUCT;

    // toolbar titles respected to selected nav menu item
    private String[] activityTitles;

    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_main);

        // Session manager instance
        session = new SessionManager(getApplicationContext());
        if(!session.isLoggedIn()){
            Intent loginIntent = new Intent(MainActivity.this, LoginActivity.class);
            MainActivity.this.startActivity(loginIntent);
            finish();
        }

        user            = session.getUserDetails();

        toolbar         = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mHandler        = new Handler();

        drawer          = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView  = (NavigationView) findViewById(R.id.nav_view);

        // Navigation view header
        navHeader       = navigationView.getHeaderView(0);
        txtName         = (TextView) navHeader.findViewById(R.id.navName);
        txtEmail        = (TextView) navHeader.findViewById(R.id.navEmail);
        imgProfile      = (ImageView) navHeader.findViewById(R.id.navPropict);

        // load toolbar titles from string resources
        activityTitles  = getResources().getStringArray(R.array.nav_item_activity_titles);

        if(session.isLoggedIn()) {
            userType        = Integer.parseInt( user.get( SessionManager.KEY_TYPE ) );
            // load nav menu header data
            loadNavHeader();
        }

        // initializing navigation menu
        setUpNavigationView();

        Menu menu = navigationView.getMenu();
        if( userType == 1 ){
            menu.findItem( R.id.nav_notifications ).setVisible( false );
            menu.findItem( R.id.nav_uploadProduct ).setVisible( false );
            menu.findItem( R.id.nav_reseller ).setVisible( false );
            menu.findItem( R.id.nav_shipment ).setVisible( false );
        }
        menu.findItem( R.id.nav_messages ).setVisible( false );
        menu.findItem( R.id.nav_target ).setVisible( false );
        menu.findItem( R.id.nav_favorite ).setVisible( false );
        menu.findItem( R.id.nav_notes ).setVisible( false );
        menu.findItem( R.id.nav_customer ).setVisible( false );

        if (savedInstanceState == null) {

            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                String itemIndex= extras.getString( "navItemIndex" ).equals( "null" ) ? "0" : extras.getString( "navItemIndex" );
                navItemIndex    = Integer.parseInt( itemIndex );
                CURRENT_TAG     = extras.getString( "CURRENT_TAG" );
                loadHomeFragment();
            } else {
                navItemIndex    = 0;
                CURRENT_TAG     = TAG_NEW_PRODUCT;
                loadHomeFragment();
            }

        }

    }

    /***
     * Load navigation menu header information
     * like background image, profile image
     * name, website, notifications action view (dot)
     */
    private void loadNavHeader() {

        // name, website
        txtName.setText(user.get( SessionManager.KEY_NAME ));
        txtEmail.setText(user.get( SessionManager.KEY_EMAIL ));

        if(!user.get( SessionManager.KEY_PROPICT ).isEmpty()){
            // Loading profile image
            Glide.with(this).load( APIUtils.BASE_URL + "/show_image/" + user.get( SessionManager.KEY_PROPICT ))
                    .crossFade()
                    .thumbnail(0.5f)
                    .bitmapTransform(new CircleTransform(this))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgProfile);
        }

    }

    /***
     * Returns respected fragment that user
     * selected from navigation menu
     */
    private void loadHomeFragment() {
        // selecting appropriate nav menu item
        selectNavMenu();

        // set toolbar title
        setToolbarTitle();

        // if user select the current navigation menu again, don't do anything
        // just close the navigation drawer
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            drawer.closeDrawers();

            return;
        }

        // Sometimes, when fragment has huge data, screen seems hanging
        // when switching between navigation menus
        // So using runnable, the fragment is loaded with cross fade effect
        // This effect can be seen in GMail app
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments
                Fragment fragment = getHomeFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                fragmentTransaction.commitAllowingStateLoss();
            }
        };

        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }

        //Closing drawer on item click
        drawer.closeDrawers();

        // refresh toolbar menu
        invalidateOptionsMenu();
    }

    private Fragment getHomeFragment() {
        switch (navItemIndex) {
            case 0:
                // New Product
                NewProductFragment newProductFragment = new NewProductFragment();
                return newProductFragment;
            case 1:
                // Messages
                MessagesFragment messagesFragment = new MessagesFragment();
                return messagesFragment;
            case 2:
                // Order
                OrderFragment orderFragment = new OrderFragment();
                return orderFragment;
            case 3:
                // Target
                TargetFragment targetFragment = new TargetFragment();
                return targetFragment;
            case 4:
                // Notifications
                NotificationsFragment notificationsFragment = new NotificationsFragment();
                return notificationsFragment;
            case 5:
                // Favorite
                FavoriteFragment favoriteFragment = new FavoriteFragment();
                return favoriteFragment;
            case 6:
                // Upload Product
                UploadProductFragment uploadProductFragment = new UploadProductFragment();
                return uploadProductFragment;
            case 7:
                // Supplier
                SupplierFragment supplierFragment = new SupplierFragment();
                return supplierFragment;
            case 8:
                // Reseller
                ResellerFragment resellerFragment = new ResellerFragment();
                return resellerFragment;
            case 9:
                // Customer
                CustomerFragment customerFragment = new CustomerFragment();
                return customerFragment;
            case 10:
                // Confirm Payment
                ConfirmPaymentFragment confirmPaymentFragment = new ConfirmPaymentFragment();
                return confirmPaymentFragment;
            case 11:
                // shipment
                ShipmentFragment shipmentFragment = new ShipmentFragment();
                return shipmentFragment;
            case 12:
                // Notes
                NotesFragment notesFragment = new NotesFragment();
                return notesFragment;
            case 13:
                // Settings
                SettingsFragment settingsFragment = new SettingsFragment();
                return settingsFragment;
            default:
                return new NewProductFragment();
        }
    }

    private void setToolbarTitle() {
        getSupportActionBar().setTitle(activityTitles[navItemIndex]);
    }

    private void selectNavMenu() {
        navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }

    private void setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.nav_newProduct:
                        navItemIndex = 0;
                        CURRENT_TAG = TAG_NEW_PRODUCT;
                        break;
                    case R.id.nav_messages:
                        navItemIndex = 1;
                        CURRENT_TAG = TAG_MESSAGES;
                        break;
                    case R.id.nav_order:
                        navItemIndex = 2;
                        CURRENT_TAG = TAG_ORDER;
                        break;
                    case R.id.nav_target:
                        navItemIndex = 3;
                        CURRENT_TAG = TAG_TARGET;
                        break;
                    case R.id.nav_notifications:
                        navItemIndex = 4;
                        CURRENT_TAG = TAG_NOTIFICATIONS;
                        break;
                    case R.id.nav_favorite:
                        navItemIndex = 5;
                        CURRENT_TAG = TAG_FAVORITE;
                        break;
                    case R.id.nav_uploadProduct:
                        navItemIndex = 6;
                        CURRENT_TAG = TAG_UPLOAD_PRODUCT;
                        break;
                    case R.id.nav_supplier:
                        navItemIndex = 7;
                        CURRENT_TAG = TAG_SUPPLIER;
                        break;
                    case R.id.nav_reseller:
                        navItemIndex = 8;
                        CURRENT_TAG = TAG_RESELLER;
                        break;
                    case R.id.nav_customer:
                        navItemIndex = 9;
                        CURRENT_TAG = TAG_CUSTOMER;
                        break;
                    case R.id.nav_confirmPayment:
                        navItemIndex = 10;
                        CURRENT_TAG = TAG_CONFIRM_PAYMENT;
                        break;
                    case R.id.nav_shipment:
                        navItemIndex = 11;
                        CURRENT_TAG = TAG_SHIPMENT;
                        break;
                    case R.id.nav_notes:
                        navItemIndex = 12;
                        CURRENT_TAG = TAG_NOTES;
                        break;
                    case R.id.nav_settings:
                        navItemIndex = 13;
                        CURRENT_TAG = TAG_SETTINGS;
                        break;
                    case R.id.nav_logout:
                        session.logoutUser();
                        finish();
                    default:
                        navItemIndex = 0;
                }

                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);

                loadHomeFragment();

                return true;
            }
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.addDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }

        // checking if user is on other navigation menu
        // rather than home
        if (navItemIndex != 0) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_NEW_PRODUCT;
            loadHomeFragment();
            return;
        }

        super.onBackPressed();
    }

}
