package id.resisten.app.orders;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import id.resisten.app.MainActivity;
import id.resisten.app.R;
import id.resisten.app.retrofit.APIServices;
import id.resisten.app.retrofit.APIUtils;
import id.resisten.app.retrofit.ROAPIServices;
import id.resisten.app.retrofit.ROAPIUtils;
import id.resisten.app.spinner.CityDataModel;
import id.resisten.app.spinner.CourierDataModel;
import id.resisten.app.spinner.CourierServicesDataModel;
import id.resisten.app.spinner.ProvinceDataModel;
import id.resisten.app.spinner.SubdistrictDataModel;
import id.resisten.app.users.CircleTransform;
import id.resisten.app.users.LoginActivity;
import id.resisten.app.users.SessionManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrdersActivity extends AppCompatActivity {

    // Session Manager class
    private SessionManager session;
    private HashMap<String, String> user;

    private int count,
            weight,
            totalWeight,
            cost,
            totalPrice,
            grandTotal,
            destProvinceId,
            destCityId;

    private String suppPropict,
            suppName,
            provinceId,
            cityId,
            subdistrictId,
            stock,
            productDate,
            productId,
            productName,
            productPrice,
            productPriceFormatted,
            productCommision,
            productCommisionFormatted,
            productWeight,
            productThumb,
            productDescription,
            cDestination,
            cCourierId,
            cCourierCode,
            cCourierServiceCode,
            etCount,
            totalCount,
            mTotalWeight,
            destProvinceName,
            destCityName,
            destSubdistrictName;

    private ScrollView svOrder;

    private ImageView ivSuppPropict,
            ivProductThumb,
            ivDecBtn,
            ivIncBtn;

    private TextView tvSuppName,
            tvProductDate,
            tvProductName,
            tvProductPrice,
            tvProductStock,
            tvOrderTotalCount,
            tvOrderTotalWeight,
            tvOrderTotalPrice,
            tvOrderTotalShippingCost,
            tvOrderGrandTotal;

    private EditText etCustomerName,
            etCustomerPhone,
            etShippingAddress,
            etProductCount,
            etDescription;

    private Spinner provinceSpinner,
            citySpinner,
            subdistrictSpinner,
            courierSpinner,
            courierServicesSpinner;

    private Button btOrderSubmit;

    private DecimalFormat IndonesianCurrency;
    private DecimalFormatSymbols formatRp;

    private List<ProvinceDataModel> provinceList;
    private ArrayAdapter<ProvinceDataModel> provinceAdapter;

    private List<CityDataModel> cityList;
    private ArrayAdapter<CityDataModel> cityAdapter;

    private List<SubdistrictDataModel> subdistrictList;
    private ArrayAdapter<SubdistrictDataModel> subdistrictAdapter;

    private List<CourierDataModel> courierList;
    private ArrayAdapter<CourierDataModel> courierAdapter;

    private List<CourierServicesDataModel> courierServicesList;
    private ArrayAdapter<CourierServicesDataModel> courierServicesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);

        // Session manager instance
        session = new SessionManager(OrdersActivity.this);
        if(!session.isLoggedIn()){
            Intent loginIntent = new Intent(OrdersActivity.this, LoginActivity.class);
            startActivity(loginIntent);
            finish();
        }

        user = session.getUserDetails();

        getSupportActionBar().setTitle( getResources().getString( R.string.navOrder ) );
        getSupportActionBar().setDisplayHomeAsUpEnabled( true );

        // Init UI
        init_UI();

        // Setup Product Preview
        setup_product_preview();

        // Setup province spinner
        setup_province_spinner();

        // Setup courier spinner
        setup_courier_spinner();

    }

    /*
     * Initializing UI elements
     */
    private void init_UI() {

        svOrder         = (ScrollView) findViewById( R.id.svOrder );

        svOrder.setDescendantFocusability( ViewGroup.FOCUS_BEFORE_DESCENDANTS );
        svOrder.setFocusable(true);
        svOrder.setFocusableInTouchMode(true);
        svOrder.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.requestFocusFromTouch();
                return false;
            }
        });

        ivSuppPropict       = (ImageView) findViewById( R.id.ivDetailsSuppPropict );
        ivProductThumb      = (ImageView) findViewById( R.id.ivOrderThumbnail );
        ivDecBtn            = (ImageView) findViewById( R.id.ivOrderDecrBtn );
        ivIncBtn            = (ImageView) findViewById( R.id.ivOrderIncBtn );

        tvSuppName          = (TextView) findViewById( R.id.tvDetailsSuppName );
        tvProductDate       = (TextView) findViewById( R.id.tvOrderProductDate );
        tvProductName       = (TextView) findViewById( R.id.tvDetailsProductName );
        tvProductPrice      = (TextView) findViewById( R.id.tvOrderProductPrice );
        tvProductStock      = (TextView) findViewById( R.id.tvOrderStock);

        tvOrderTotalCount   = (TextView) findViewById( R.id.tvOrderTotalCount );
        tvOrderTotalWeight  = (TextView) findViewById( R.id.tvOrderTotalWeight );
        tvOrderTotalShippingCost = (TextView) findViewById( R.id.tvOrderTotalShippingCost );
        tvOrderTotalPrice   = (TextView) findViewById( R.id.tvOrderTotalPrice );
        tvOrderGrandTotal   = (TextView) findViewById( R.id.tvOrderGrandTotal );

        etCustomerName      = (EditText) findViewById( R.id.etOrderCustomerName );
        etCustomerPhone     = (EditText) findViewById( R.id.etOrderCustomerPhone );
        etShippingAddress   = (EditText) findViewById( R.id.etOrderShippingAddress );
        etProductCount      = (EditText) findViewById( R.id.etOrderCount );
        etDescription       = (EditText) findViewById( R.id.etOrderDescription );

        btOrderSubmit       = (Button) findViewById( R.id.btOrderSubmit );

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            suppPropict                 = extras.getString( "suppPropict" );
            suppName                    = extras.getString( "suppName" );
            provinceId                  = extras.getString( "provinceID" );
            cityId                      = extras.getString( "cityID" );
            subdistrictId               = extras.getString( "subdistrictID" );
            stock                       = extras.getString( "stock" );
            productDate                 = extras.getString( "productDate" );
            productId                   = extras.getString( "productId" );
            productName                 = extras.getString( "productName" );
            productPrice                = extras.getString( "productPrice" );
            productPriceFormatted       = extras.getString( "productPriceFormatted" );
            productCommision            = extras.getString( "productCommision" );
            productCommisionFormatted   = extras.getString( "productCommisionFormatted" );
            productWeight               = extras.getString( "productWeight" );
            productThumb                = extras.getString( "productThumb" );
            productDescription          = extras.getString( "productDescription" );
        }

        ivDecBtn.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String valStock = etProductCount.getText().toString();
                int stock;

                if(valStock.isEmpty()){
                    stock = 0;
                } else {
                    stock = Integer.parseInt( valStock );
                    if(stock > 0){
                        stock--;
                    }
                }
                String txtStock = Integer.toString( stock );
                etProductCount.setText( txtStock );
            }
        } );

        ivIncBtn.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String valStock = etProductCount.getText().toString();
                int stock;

                if(valStock.isEmpty()){
                    stock = 0;
                } else {
                    stock = Integer.parseInt( valStock );
                }

                stock++;
                String txtStock = Integer.toString( stock );
                etProductCount.setText( txtStock );
            }
        } );

        etProductCount.addTextChangedListener( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() > 0){
                    courierSpinner.setSelection( 0 );
                }
            }
        } );

        btOrderSubmit.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                post_order();
            }
        } );
    }

    /*
     * Setup Product Preview
     */
    private void setup_product_preview() {

        // Set supplier profile picture
        if(!suppPropict.equals( "null" )){
            Glide.with( OrdersActivity.this ).load( APIUtils.BASE_URL + "/show_image/" + suppPropict )
                    .crossFade()
                    .thumbnail( 0.5f )
                    .bitmapTransform( new CircleTransform( OrdersActivity.this ) )
                    .diskCacheStrategy( DiskCacheStrategy.ALL )
                    .into( ivSuppPropict );
        }

        tvSuppName.setText( suppName ); // Set supplier name
        tvProductDate.setText( productDate ); // Set product date
        tvProductName.setText( productName ); // Set product name
        tvProductPrice.setText( productPriceFormatted ); // Set product price
        tvProductStock.setText( stock );

        // Set product thumbnail
        Glide.with( OrdersActivity.this ).load( APIUtils.BASE_URL + "/show_image/" + productThumb )
                .into( ivProductThumb );

    }

    /*
     * Setup Province Spinner
     */
    private void setup_province_spinner() {

        provinceSpinner = (Spinner) findViewById( R.id.spOrderProvince );

        provinceList    = new ArrayList<>();
        provinceList.add( new ProvinceDataModel( 0, getResources().getString( R.string.provinceSpinnerHint ) ) );

        ROAPIServices roApiServices = ROAPIUtils.getROAPIService();
        Call<ResponseBody> result   = roApiServices.getProvinces();
        result.enqueue( new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {

                    JSONObject jsonResponse = new JSONObject( response.body().string() );
                    JSONObject rajaongkir   = jsonResponse.getJSONObject( "rajaongkir" );
                    JSONObject status       = rajaongkir.getJSONObject( "status" );
                    int statusCode          = status.getInt( "code" );

                    if(statusCode == 200){

                        JSONArray arrayResults = rajaongkir.getJSONArray( "results" );
                        if(arrayResults.length() > 0){
                            for(int i = 0; i < arrayResults.length(); i++){

                                JSONObject object = arrayResults.getJSONObject( i );
                                ProvinceDataModel provinceData = new ProvinceDataModel(
                                        object.getInt( "province_id" ),
                                        object.getString( "province" )
                                );

                                provinceList.add( provinceData );

                            }
                        }

                        provinceAdapter.notifyDataSetChanged();

                    } else {
                        String message = status.getString( "decription" );

                        AlertDialog.Builder builder = new AlertDialog.Builder( OrdersActivity.this );
                        builder.setMessage( message )
                                .setNegativeButton( "Retry", null )
                                .create()
                                .show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
            }
        } );

        provinceAdapter = new ArrayAdapter<>( OrdersActivity.this, android.R.layout.simple_spinner_item, provinceList );
        provinceAdapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );

        provinceSpinner.setAdapter( provinceAdapter );

        provinceSpinner.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                courierSpinner.setSelection( 0 );

                if(position > 0){
                    destProvinceId      = provinceList.get( position ).getId();
                    destProvinceName    = provinceList.get( position ).getProvince();
                    setup_city_spinner(destProvinceId);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        } );

    }

    /*
     * Setup City Spinner
     */
    private void setup_city_spinner(final int provinceId) {

        citySpinner = (Spinner) findViewById( R.id.spOrderCity );

        cityList    = new ArrayList<>();
        cityList.add( new CityDataModel( 0, 0, "", getResources().getString( R.string.citySpinnerHint1 ), getResources().getString( R.string.citySpinnerHint2 ), "" ) );

        ROAPIServices roApiServices = ROAPIUtils.getROAPIService();
        Call<ResponseBody> result   = roApiServices.getCities(provinceId);
        result.enqueue( new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {

                    JSONObject jsonResponse = new JSONObject( response.body().string() );
                    JSONObject rajaongkir   = jsonResponse.getJSONObject( "rajaongkir" );
                    JSONObject status       = rajaongkir.getJSONObject( "status" );
                    int statusCode          = status.getInt( "code" );

                    if(statusCode == 200){

                        JSONArray arrayResults = rajaongkir.getJSONArray( "results" );
                        if(arrayResults.length() > 0){
                            for(int i = 0; i < arrayResults.length(); i++){

                                JSONObject object = arrayResults.getJSONObject( i );
                                CityDataModel cityData = new CityDataModel(
                                        object.getInt( "city_id" ),
                                        object.getInt( "province_id" ),
                                        object.getString( "province" ),
                                        object.getString( "type" ),
                                        object.getString( "city_name" ),
                                        object.getString( "postal_code" )
                                );

                                cityList.add( cityData );

                            }
                        }

                        cityAdapter.notifyDataSetChanged();

                    } else {
                        String message = status.getString( "decription" );

                        AlertDialog.Builder builder = new AlertDialog.Builder( OrdersActivity.this );
                        builder.setMessage( message )
                                .setNegativeButton( "Retry", null )
                                .create()
                                .show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
            }
        } );

        cityAdapter = new ArrayAdapter<>( OrdersActivity.this, android.R.layout.simple_spinner_item, cityList );
        cityAdapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );

        citySpinner.setAdapter( cityAdapter );

        citySpinner.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                courierSpinner.setSelection( 0 );

                if(position > 0) {

                    destCityId      = cityList.get( position ).getId();
                    destCityName    = cityList.get( position ).getCityName();
                    setup_subdistrict_spinner( destCityId );

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        } );

    }

    /*
     * Setup Subdistrict Spinner
     */
    private void setup_subdistrict_spinner(final int cityId) {

        subdistrictSpinner = (Spinner) findViewById( R.id.spOrderSubdistrict );

        subdistrictList    = new ArrayList<>();
        subdistrictList.add( new SubdistrictDataModel( 0, 0, "", 0, "", "", getResources().getString( R.string.subdistrictSpinnerHint ) ) );

        ROAPIServices roApiServices = ROAPIUtils.getROAPIService();
        Call<ResponseBody> result   = roApiServices.getSubdistrict(cityId);
        result.enqueue( new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {

                    JSONObject jsonResponse = new JSONObject( response.body().string() );
                    JSONObject rajaongkir   = jsonResponse.getJSONObject( "rajaongkir" );
                    JSONObject status       = rajaongkir.getJSONObject( "status" );
                    int statusCode          = status.getInt( "code" );

                    if(statusCode == 200){

                        JSONArray arrayResults = rajaongkir.getJSONArray( "results" );
                        if(arrayResults.length() > 0){
                            for(int i = 0; i < arrayResults.length(); i++){

                                JSONObject object = arrayResults.getJSONObject( i );
                                SubdistrictDataModel subdistrictData = new SubdistrictDataModel(
                                        object.getInt( "subdistrict_id" ),
                                        object.getInt( "province_id" ),
                                        object.getString( "province" ),
                                        object.getInt( "city_id" ),
                                        object.getString( "city" ),
                                        object.getString( "type" ),
                                        object.getString( "subdistrict_name" )
                                );

                                subdistrictList.add( subdistrictData );

                            }
                        }

                        subdistrictAdapter.notifyDataSetChanged();

                    } else {
                        String message = status.getString( "decription" );

                        AlertDialog.Builder builder = new AlertDialog.Builder( OrdersActivity.this );
                        builder.setMessage( message )
                                .setNegativeButton( "Retry", null )
                                .create()
                                .show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
            }
        } );

        subdistrictAdapter = new ArrayAdapter<>( OrdersActivity.this, android.R.layout.simple_spinner_item, subdistrictList );
        subdistrictAdapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );

        subdistrictSpinner.setAdapter( subdistrictAdapter );

        subdistrictSpinner.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                courierSpinner.setSelection( 0 );

                if(position > 0){
                    destSubdistrictName = subdistrictList.get( position ).getSubdistrictName();
                    cDestination = Integer.toString( subdistrictList.get( position ).getId() );
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        } );

    }

    /*
     * Setup Courier Spinner
     */
    private void setup_courier_spinner() {
        courierSpinner = (Spinner) findViewById( R.id.spOrderCourier );

        courierList    = new ArrayList<>();
        courierList.add( new CourierDataModel( 0, getResources().getString( R.string.courierSpinnerHint ), "", "" ) );

        APIServices apiServices     = APIUtils.getAPIService();
        Call<ResponseBody> result   = apiServices.getShippingAgents(user.get(SessionManager.KEY_TOKEN));
        result.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {

                    JSONObject jsonResponse = new JSONObject( response.body().string() );
                    boolean status          = jsonResponse.getBoolean( "status" );

                    if(status){

                        JSONArray arrayResults = jsonResponse.getJSONArray( "message" );
                        if(arrayResults.length() > 0){
                            for(int i = 0; i < arrayResults.length(); i++){

                                JSONObject object = arrayResults.getJSONObject( i );
                                CourierDataModel courierData = new CourierDataModel(
                                        object.getInt( "id" ),
                                        object.getString( "name" ),
                                        object.getString( "code" ),
                                        object.getString( "description" )
                                );

                                courierList.add( courierData );

                            }
                        }

                        courierAdapter.notifyDataSetChanged();

                    } else {
                        String message = jsonResponse.getString( "message" );

                        AlertDialog.Builder builder = new AlertDialog.Builder( OrdersActivity.this );
                        builder.setMessage( message )
                                .setNegativeButton( "Retry", null )
                                .create()
                                .show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
            }
        });

        courierAdapter = new ArrayAdapter<>( OrdersActivity.this, android.R.layout.simple_spinner_item, courierList );
        courierAdapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );

        courierSpinner.setAdapter( courierAdapter );

        courierSpinner.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cCourierId      = Integer.toString( courierList.get( position ).getId() );
                cCourierCode    = courierList.get( position ).getCode();
                update_order_data();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        } );

    }

    /*
     * Update order detail and courier service spinner whenever spinners or count changes
     */
    private void update_order_data()
    {

        tvOrderTotalCount.setText( "" );
        tvOrderTotalWeight.setText( "" );
        tvOrderTotalShippingCost.setText( "" );
        tvOrderTotalPrice.setText( "" );
        tvOrderGrandTotal.setText( "" );

        courierServicesSpinner  = (Spinner) findViewById( R.id.spOrderCourierServices );

        courierServicesList     = new ArrayList<>();
        courierServicesList.add( new CourierServicesDataModel( getResources().getString( R.string.courierServiceSpinnerHint ), 0 ) );

        etCount  = etProductCount.getText().toString().trim();
        weight      = Integer.parseInt(productWeight);
        count       = 1;
        if(etCount.length() > 0){
            count     = Integer.parseInt(etCount);
        }
        totalWeight = weight * count;

        if(totalWeight > 0 && cCourierCode.length() > 0 && cDestination != null ) {

            HashMap<String, String> params = new HashMap<>();
            params.put( "origin", cityId );
            params.put( "originType", "city" );
            params.put( "destination", cDestination );
            params.put( "destinationType", "subdistrict" );
            params.put( "weight", totalWeight + "" );
            params.put( "courier", cCourierCode );

            ROAPIServices apiServices = ROAPIUtils.getROAPIService();
            Call<ResponseBody> result = apiServices.getShippingCost( params );
            result.enqueue( new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {

                        JSONObject jsonResponse = new JSONObject( response.body().string() );
                        JSONObject rajaongkir = jsonResponse.getJSONObject( "rajaongkir" );
                        JSONObject status = rajaongkir.getJSONObject( "status" );
                        int statusCode = status.getInt( "code" );

                        if (statusCode == 200) {

                            JSONArray arrayResults = rajaongkir.getJSONArray( "results" );
                            if (arrayResults.length() > 0) {
                                for (int i = 0; i < arrayResults.length(); i++) {

                                    JSONObject object = arrayResults.getJSONObject( i );
                                    JSONArray arrayCosts = object.getJSONArray( "costs" );
                                    if (arrayCosts.length() > 0) {

                                        for (int n = 0; n < arrayCosts.length(); n++) {

                                            JSONObject objectCosts = arrayCosts.getJSONObject( n );
                                            String serviceCode = objectCosts.getString( "service" );
                                            JSONArray arrayCost = objectCosts.getJSONArray( "cost" );
                                            JSONObject costData = arrayCost.getJSONObject( 0 );
                                            int costValue = costData.getInt( "value" );

                                            CourierServicesDataModel courierServicesDataModel = new CourierServicesDataModel( serviceCode, costValue );
                                            courierServicesList.add( courierServicesDataModel );

                                        }

                                    }

                                }

                                courierServicesAdapter.notifyDataSetChanged();

                            }

                        } else {
                            String message = status.getString( "decription" );

                            AlertDialog.Builder builder = new AlertDialog.Builder( OrdersActivity.this );
                            builder.setMessage( message )
                                    .setNegativeButton( "Retry", null )
                                    .create()
                                    .show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    t.printStackTrace();
                }
            } );

        } else {
            courierServicesList.clear();
        }

        courierServicesAdapter = new ArrayAdapter<>( OrdersActivity.this, android.R.layout.simple_spinner_item, courierServicesList );
        courierServicesAdapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );

        courierServicesSpinner.setAdapter( courierServicesAdapter );

        courierServicesSpinner.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                cCourierServiceCode = courierServicesList.get( position ).getService();

                totalCount      = count + "";
                mTotalWeight    = totalWeight + " g";
                cost            = courierServicesList.get( position ).getCost();
                totalPrice      = (Integer.parseInt( productPrice ) * count);
                grandTotal      = cost + totalPrice;

                tvOrderTotalCount.setText( totalCount );
                tvOrderTotalWeight.setText( mTotalWeight );
                tvOrderTotalShippingCost.setText( IndonesianFormat( cost ) );
                tvOrderTotalPrice.setText( IndonesianFormat( totalPrice ) );
                tvOrderGrandTotal.setText( IndonesianFormat( grandTotal ) );
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        } );

    }

    /*
     * Post order process
     */
    private void post_order() {

        final ProgressDialog spinner = new ProgressDialog( OrdersActivity.this );
        spinner.setMessage( getResources().getString( R.string.pleaseWait ) );
        spinner.setCancelable( false );
        spinner.show();

        HashMap<String, String> params = new HashMap<>();
        params.put( "api_token", user.get( SessionManager.KEY_TOKEN ) );
        params.put( "product_id", productId );
        params.put( "count", count + "" );
        params.put( "total_weight", totalWeight + "" );
        params.put( "total_price", totalPrice + "" );
        params.put( "description", etDescription.getText().toString() );
        params.put( "shipping_province_id", destProvinceId + "" );
        params.put( "shipping_province_name", destProvinceName );
        params.put( "shipping_city_id", destCityId + "" );
        params.put( "shipping_city_name", destCityName );
        params.put( "shipping_subdistrict_id", cDestination );
        params.put( "shipping_subdistrict_name", destSubdistrictName );
        params.put( "shipping_cost", cost + "" );
        params.put( "shipping_name", etCustomerName.getText().toString() );
        params.put( "shipping_phone", etCustomerPhone.getText().toString() );
        params.put( "shipping_address", etShippingAddress.getText().toString() );
        params.put( "shipping_agents_id", cCourierId );
        params.put( "shipping_service_code", cCourierServiceCode );
        params.put( "grand_total", grandTotal + "" );

        APIServices apiServices     = APIUtils.getAPIService();
        Call<ResponseBody> result   = apiServices.postOrder( params );
        result.enqueue( new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {

                    spinner.dismiss();

                    JSONObject jsonResponse = new JSONObject( response.body().string() );
                    boolean status          = jsonResponse.getBoolean( "status" );
                    String message          = jsonResponse.getString( "message" );

                    if(status){

                        Intent intent = new Intent(OrdersActivity.this, OrderSuccessActivity.class);
                        intent.putExtra( "order_id", jsonResponse.getString( "order_id" ) );
                        intent.putExtra( "seller_id", jsonResponse.getString( "seller_id" ) );
                        intent.putExtra( "total_payment", IndonesianFormat( grandTotal ) );
                        OrdersActivity.this.startActivity( intent );
                        OrdersActivity.this.finish();

                    } else {

                        AlertDialog.Builder builder = new AlertDialog.Builder( OrdersActivity.this );
                        builder.setMessage( message )
                                .setNegativeButton( "Retry", null )
                                .create()
                                .show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
            }
        } );

    }

    protected String IndonesianFormat(final int number) {

        IndonesianCurrency = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol( "Rp. " );
        formatRp.setMonetaryDecimalSeparator( ',' );
        formatRp.setGroupingSeparator( '.' );

        IndonesianCurrency.setDecimalFormatSymbols( formatRp );

        return IndonesianCurrency.format( number );
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
