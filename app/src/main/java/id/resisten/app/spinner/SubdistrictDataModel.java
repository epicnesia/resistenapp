package id.resisten.app.spinner;

/**
 * Created by LEKTOP on 24-Apr-17.
 */

public class SubdistrictDataModel {

    private int id, provinceId;
    private String province;
    private int cityId;
    private String city;
    private String type;
    private String subdistrictName;

    public SubdistrictDataModel(int id, int provinceId, String province, int cityId, String city, String type, String subdistrictName) {
        this.id = id;
        this.provinceId = provinceId;
        this.province = province;
        this.cityId = cityId;
        this.city = city;
        this.type = type;
        this.subdistrictName = subdistrictName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(int provinceId) {
        this.provinceId = provinceId;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSubdistrictName() {
        return subdistrictName;
    }

    public void setSubdistrictName(String subdistrictName) {
        this.subdistrictName = subdistrictName;
    }

    @Override
    public String toString() {
        return subdistrictName;
    }
}
