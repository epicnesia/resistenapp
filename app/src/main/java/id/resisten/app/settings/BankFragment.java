package id.resisten.app.settings;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import id.resisten.app.R;
import id.resisten.app.retrofit.APIServices;
import id.resisten.app.retrofit.APIUtils;
import id.resisten.app.users.LoginActivity;
import id.resisten.app.users.SessionManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class BankFragment extends Fragment {

    // Session Manager class
    private SessionManager session;
    private HashMap<String, String> user;

    private int page;

    private ProgressBar progressBar;

    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private BanksAdapter banksAdapter;
    private List<BanksModel> banksList;

    private FloatingActionButton fabAddBank;

    public BankFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );

        // Session manager instance
        session = new SessionManager(getActivity().getApplicationContext());
        if(!session.isLoggedIn()){
            Intent loginIntent = new Intent(this.getActivity(), LoginActivity.class);
            this.getActivity().startActivity(loginIntent);
            getActivity().finish();
        }

        user = session.getUserDetails();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate( R.layout.fragment_setting_bank, container, false );
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        progressBar = (ProgressBar) view.findViewById( R.id.pbSettingBank );

        page = 1;

        recyclerView    = (RecyclerView) view.findViewById( R.id.rvSettingBanksList );
        recyclerView.setHasFixedSize( true );
        banksList       = new ArrayList<>();

        load_data(page);

        layoutManager = new LinearLayoutManager( getActivity() );
        recyclerView.setLayoutManager( layoutManager );

        banksAdapter = new BanksAdapter(getActivity(), banksList);
        recyclerView.setAdapter(banksAdapter);

        recyclerView.addOnScrollListener( new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if(layoutManager.findLastCompletelyVisibleItemPosition() == banksList.size()-1){
                    load_data( ++page );
                }
            }
        } );

        fabAddBank = (FloatingActionButton) view.findViewById( R.id.fabAddBanks );
        fabAddBank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addBankIntent = new Intent(getActivity(), AddBankActivity.class);
                getActivity().startActivity(addBankIntent);
            }
        });

    }

    private void load_data(final int page) {

        AsyncTask<Integer, Void, Void> task = new AsyncTask<Integer, Void, Void>() {
            @Override
            protected Void doInBackground(Integer... params) {

                APIServices apiServices = APIUtils.getAPIService();
                Call<ResponseBody> result = apiServices.getBanks( user.get( SessionManager.KEY_TOKEN), page );
                try {

                    Response<ResponseBody> response = result.execute();

                    JSONObject jsonResponse = new JSONObject( response.body().string() );
                    boolean status = jsonResponse.getBoolean( "status" );

                    if(status){

                        JSONObject message = jsonResponse.getJSONObject( "message" );

                        JSONArray array = message.getJSONArray( "data" );
                        for(int i=0;i<array.length();i++){

                            JSONObject object = array.getJSONObject( i );

                            BanksModel data = new BanksModel (
                                    object.getInt( "id" ),
                                    object.getString( "bank_name" ),
                                    object.getString( "account_number" ),
                                    object.getString( "account_name" )
                            );

                            banksList.add( data );

                        }

                    } else {

                        String message = jsonResponse.getString( "message" );

                        AlertDialog.Builder builder = new AlertDialog.Builder( getActivity() );
                        builder.setMessage( message )
                                .setNegativeButton( "Retry", null )
                                .create()
                                .show();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid){
                progressBar.setVisibility( View.GONE );
                banksAdapter.notifyDataSetChanged();
            }
        };

        task.execute();

    }

}
