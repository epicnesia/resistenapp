package id.resisten.app.shipments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import id.resisten.app.MainActivity;
import id.resisten.app.R;
import id.resisten.app.retrofit.APIServices;
import id.resisten.app.retrofit.APIUtils;
import id.resisten.app.users.LoginActivity;
import id.resisten.app.users.SessionManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShipmentDetailsActivity extends AppCompatActivity {

    // Session Manager class
    private SessionManager session;
    private HashMap<String, String> user;

    private String orderId,
            productName,
            sellerName,
            buyerName,
            description,
            count,
            totalWeight,
            totalPrice,
            totalShippingCost,
            grandTotal,
            shippingAgents,
            shippingServiceCode,
            shippingName,
            shippingPhone,
            shippingProvince,
            shippingCity,
            shippingSubdistrict,
            shippingAddress,
            paymentReceipt;

    private TextView tvOrderId,
            tvProductName,
            tvSellerName,
            tvBuyerName,
            tvDescription,
            tvCount,
            tvTotalWeight,
            tvTotalPrice,
            tvTotalShippingCost,
            tvGrandTotal,
            tvShippingAgents,
            tvShippingServiceCode,
            tvShippingName,
            tvShippingPhone,
            tvShippingProvince,
            tvShippingCity,
            tvShippingSubdistrict,
            tvShippingAddress;

    private EditText etShippingCode;

    private Button btOrderProcess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_shipment_details );

        // Session manager instance
        session = new SessionManager(ShipmentDetailsActivity.this);
        if(!session.isLoggedIn()){
            Intent loginIntent = new Intent(ShipmentDetailsActivity.this, LoginActivity.class);
            startActivity(loginIntent);
            finish();
        }

        user = session.getUserDetails();

        getSupportActionBar().setTitle( getResources().getString( R.string.orderDetail ) );
        getSupportActionBar().setDisplayHomeAsUpEnabled( true );

        // Init UI
        init_UI();

        // Setup order details preview
        setup_order_details();

        btOrderProcess.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                update_order();
            }
        } );
    }

    /*
     * Initializing UI elements
     */
    private void init_UI() {

        // Order details section
        tvOrderId           = (TextView) findViewById( R.id.tvOrderId );
        tvProductName       = (TextView) findViewById( R.id.tvProductName );
        tvSellerName        = (TextView) findViewById( R.id.tvSellerName );
        tvBuyerName         = (TextView) findViewById( R.id.tvBuyerName );
        tvDescription       = (TextView) findViewById( R.id.tvDescription );
        tvCount             = (TextView) findViewById( R.id.tvOrderCount );
        tvTotalWeight       = (TextView) findViewById( R.id.tvOrderTotalWeight );
        tvTotalPrice        = (TextView) findViewById( R.id.tvOrderTotalPrice );
        tvTotalShippingCost = (TextView) findViewById( R.id.tvOrderShippingCost );
        tvGrandTotal        = (TextView) findViewById( R.id.tvOrderGrandTotal );

        // Shipping details section
        tvShippingAgents        = (TextView) findViewById( R.id.tvShippingAgents );
        tvShippingServiceCode   = (TextView) findViewById( R.id.tvShippingService );
        tvShippingName          = (TextView) findViewById( R.id.tvShippingName );
        tvShippingPhone         = (TextView) findViewById( R.id.tvShippingPhone );
        tvShippingProvince      = (TextView) findViewById( R.id.tvShippingProvince );
        tvShippingCity          = (TextView) findViewById( R.id.tvShippingCity );
        tvShippingSubdistrict   = (TextView) findViewById( R.id.tvShippingSubdistrict );
        tvShippingAddress       = (TextView) findViewById( R.id.tvShippingAddress );

        // Shipping code section
        etShippingCode      = (EditText) findViewById( R.id.etShippingCode );

        // Button process
        btOrderProcess      = (Button) findViewById( R.id.btOrderDetailProcess );

        // Get intent's extras
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            orderId             = extras.getString( "orderId" );
            productName         = extras.getString( "productName" );
            sellerName          = extras.getString( "sellerName" );
            buyerName           = extras.getString( "buyerName" );
            description         = extras.getString( "description" );
            count               = extras.getString( "count" );
            totalWeight         = extras.getString( "totalWeight" );
            totalPrice          = extras.getString( "totalPrice" );
            totalShippingCost   = extras.getString( "totalShippingCost" );
            grandTotal          = extras.getString( "grandTotal" );
            shippingAgents      = extras.getString( "shippingAgents" );
            shippingServiceCode = extras.getString( "shippingServiceCode" );
            shippingName        = extras.getString( "shippingName" );
            shippingPhone       = extras.getString( "shippingPhone" );
            shippingProvince    = extras.getString( "shippingProvince" );
            shippingCity        = extras.getString( "shippingCity" );
            shippingSubdistrict = extras.getString( "shippingSubdistrict" );
            shippingAddress     = extras.getString( "shippingAddress" );
            paymentReceipt      = extras.getString( "paymentReceipt" );
        }

    }

    /*
     * Setup Order Details
     */
    private void setup_order_details() {

        // Set order details section
        tvOrderId.setText( orderId );
        tvProductName.setText( productName );
        tvSellerName.setText( sellerName );
        tvBuyerName.setText( buyerName );

        if(!description.equals( "null" )){
            tvDescription.setText( description );
        } else {
            tvDescription.setText( "-" );
        }

        tvCount.setText( count );
        tvTotalWeight.setText( totalWeight );
        tvTotalPrice.setText( totalPrice );
        tvTotalShippingCost.setText( totalShippingCost );
        tvGrandTotal.setText( grandTotal );

        // Set shipping details section
        tvShippingAgents.setText( shippingAgents );
        tvShippingServiceCode.setText( shippingServiceCode );
        tvShippingName.setText( shippingName );
        tvShippingPhone.setText( shippingPhone );
        tvShippingProvince.setText( shippingProvince );
        tvShippingCity.setText( shippingCity );
        tvShippingSubdistrict.setText( shippingSubdistrict );
        tvShippingAddress.setText( shippingAddress );

    }

    // Update order status to "ON PROCESS" because the shipping code has been submitted
    private void update_order() {

        final ProgressDialog spinner = new ProgressDialog( ShipmentDetailsActivity.this );
        spinner.setMessage( getResources().getString( R.string.pleaseWait ) );
        spinner.setCancelable( false );
        spinner.show();

        APIServices apiServices     = APIUtils.getAPIService();
        Call<ResponseBody> result   = apiServices.submitShippingCode( user.get( SessionManager.KEY_TOKEN ),
                                                                      Integer.parseInt( orderId ),
                                                                      etShippingCode.getText().toString()
                                                                    );
        result.enqueue( new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                spinner.dismiss();

                try {

                    JSONObject jsonResponse = new JSONObject( response.body().string() );
                    boolean status          = jsonResponse.getBoolean( "status" );
                    String message          = jsonResponse.getString( "message" );

                    if (status){

                        Toast.makeText( ShipmentDetailsActivity.this, message, Toast.LENGTH_LONG ).show();

                        Intent intent = new Intent( ShipmentDetailsActivity.this, MainActivity.class );
                        intent.putExtra( "navItemIndex", "2" );
                        intent.putExtra( "CURRENT_TAG", "order" );
                        intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
                        startActivity( intent );
                        finish();

                    } else {

                        AlertDialog.Builder builder = new AlertDialog.Builder( ShipmentDetailsActivity.this );
                        builder.setMessage( message )
                                .setNegativeButton( "Retry", null )
                                .create()
                                .show();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                spinner.dismiss();
                t.printStackTrace();
            }
        } );

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
