package id.resisten.app.users;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import id.resisten.app.R;
import id.resisten.app.retrofit.APIServices;
import id.resisten.app.retrofit.APIUtils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_register);

        final EditText etEmail = (EditText) findViewById(R.id.etUserEmail);
        final EditText etName = (EditText) findViewById(R.id.etUserName);
        final EditText etPhone = (EditText) findViewById(R.id.etUserPhone);
        final EditText etPass = (EditText) findViewById(R.id.etUserPass);

        final RadioGroup radioGroup = (RadioGroup) findViewById(R.id.rgUserType);

        final Button btnRegister = (Button) findViewById(R.id.regBtn);

        btnRegister.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    btnRegister.setBackgroundColor(Color.WHITE);
                    btnRegister.setTextColor(Color.RED);
                } else if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    btnRegister.setBackgroundColor(Color.RED);
                    btnRegister.setTextColor(Color.WHITE);
                }
                return false;
            }

        });

        btnRegister.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                final ProgressDialog spinner = new ProgressDialog( RegisterActivity.this );
                spinner.setMessage( "Please wait..." );
                spinner.setCancelable( false );
                spinner.show();

                final String password = etPass.getText().toString();
                final String name = etName.getText().toString();
                final String email = etEmail.getText().toString();
                final String phone = etPhone.getText().toString();

                // Get selected Radio Button Id
                int selectedId = radioGroup.getCheckedRadioButtonId();
                final RadioButton radioButton = (RadioButton) findViewById(selectedId);
                final String typeString = radioButton.getText().toString();
                int type = 1;

                if(typeString.equals("Supplier")){
                    type = 2;
                }

                String androidId = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

                HashMap<String, String> params = new HashMap<>();
                params.put("password", password);
                params.put("name", name);
                params.put("email", email);
                params.put("phone", phone);
                params.put("type", type + "");
                params.put("android_id", androidId);

                APIServices apiServices = APIUtils.getAPIService();
                Call<ResponseBody> result = apiServices.register( params );
                result.enqueue(new Callback<ResponseBody>() {

                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        spinner.dismiss();

                        try {

                            JSONObject jsonResponse = new JSONObject(response.body().string());
                            boolean status = jsonResponse.getBoolean("status");
                            String message = jsonResponse.getString("message");

                            if(status){

                                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                                intent.putExtra( "SUCCESS_MESSAGE", message );
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();

                            } else {

                                AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                                builder.setMessage(message)
                                        .setNegativeButton("Retry", null)
                                        .create()
                                        .show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        spinner.dismiss();
                        t.printStackTrace();
                    }
                });

            }

        });

        // Login link pressed
        final TextView loginLink = (TextView) findViewById(R.id.loginLink);
        loginLink.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){
                Intent registerIntent = new Intent(RegisterActivity.this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                RegisterActivity.this.startActivity(registerIntent);
                finish();
            }

        });

    }
}
