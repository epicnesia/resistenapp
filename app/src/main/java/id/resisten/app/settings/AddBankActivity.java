package id.resisten.app.settings;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import id.resisten.app.MainActivity;
import id.resisten.app.R;
import id.resisten.app.retrofit.APIServices;
import id.resisten.app.retrofit.APIUtils;
import id.resisten.app.suppliers.AddSupplierActivity;
import id.resisten.app.users.LoginActivity;
import id.resisten.app.users.SessionManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddBankActivity extends AppCompatActivity {

    // Session Manager class
    private SessionManager session;
    private HashMap<String, String> user;

    private EditText etBankName,
            etBankAccountNumber,
            etBankAccountName;

    private Button btAddBank;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_bank);

        // Session manager instance
        session = new SessionManager(AddBankActivity.this);
        if(!session.isLoggedIn()){
            Intent loginIntent = new Intent(AddBankActivity.this, LoginActivity.class);
            startActivity(loginIntent);
            finish();
        }

        user = session.getUserDetails();

        getSupportActionBar().setTitle( getResources().getString( R.string.addSupplierTitle ) );
        getSupportActionBar().setDisplayHomeAsUpEnabled( true );

        etBankName          = (EditText) findViewById( R.id.etBankName );
        etBankAccountNumber = (EditText) findViewById( R.id.etBankAccountNumber );
        etBankAccountName   = (EditText) findViewById( R.id.etBankAccountName );

        btAddBank           = (Button) findViewById( R.id.btAddBank );

        btAddBank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_bank();
            }
        });

    }

    private void add_bank() {

        final ProgressDialog spinner = new ProgressDialog( AddBankActivity.this );
        spinner.setMessage( getResources().getString( R.string.pleaseWait ) );
        spinner.setCancelable( false );
        spinner.show();

        HashMap<String, String> params = new HashMap<>();
        params.put( "api_token", user.get( SessionManager.KEY_TOKEN ) );
        params.put( "bank_name", etBankName.getText().toString() );
        params.put( "account_number", etBankAccountNumber.getText().toString() );
        params.put( "account_name", etBankAccountName.getText().toString() );

        APIServices apiServices     = APIUtils.getAPIService();
        Call<ResponseBody> result   = apiServices.addBank( params );
        result.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {

                    spinner.dismiss();

                    JSONObject jsonResponse = new JSONObject( response.body().string() );
                    boolean status          = jsonResponse.getBoolean( "status" );
                    String message          = jsonResponse.getString( "message" );

                    if(status) {

                        Toast.makeText( AddBankActivity.this, message, Toast.LENGTH_LONG ).show();

                        Intent intent = new Intent( AddBankActivity.this, MainActivity.class );
                        intent.putExtra( "navItemIndex", "13" );
                        intent.putExtra( "CURRENT_TAG", "settings" );
                        intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
                        startActivity( intent );
                        finish();

                    } else {

                        AlertDialog.Builder builder = new AlertDialog.Builder( AddBankActivity.this );
                        builder.setMessage( message )
                                .setNegativeButton( "Retry", null )
                                .create()
                                .show();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                spinner.dismiss();
                t.printStackTrace();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
