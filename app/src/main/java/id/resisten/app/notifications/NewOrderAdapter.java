package id.resisten.app.notifications;

import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;

import id.resisten.app.R;
import id.resisten.app.orders.OrderDetailsActivity;
import id.resisten.app.orders.OrdersDataModel;
import id.resisten.app.retrofit.APIUtils;
import id.resisten.app.retrofit.ROAPIServices;
import id.resisten.app.retrofit.ROAPIUtils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by LEKTOP on 14-May-17.
 */

public class NewOrderAdapter extends RecyclerView.Adapter<NewOrderAdapter.ViewHolder> {

    private Context context;
    private List<OrdersDataModel> ordersList;

    private DecimalFormat IndonesianCurrency;
    private DecimalFormatSymbols formatRp;

    public NewOrderAdapter(Context context, List<OrdersDataModel> ordersList) {
        this.context = context;
        this.ordersList = ordersList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from( parent.getContext() ).inflate( R.layout.fragment_new_order_rv, parent, false );
        return new NewOrderAdapter.ViewHolder( itemView );
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        IndonesianCurrency = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol( "Rp. " );
        formatRp.setMonetaryDecimalSeparator( ',' );
        formatRp.setGroupingSeparator( '.' );

        IndonesianCurrency.setDecimalFormatSymbols( formatRp );

        if(!ordersList.get( position ).getProductImage().equals( "null" )){
            Glide.with( context ).load( APIUtils.BASE_URL + "/show_image/" + ordersList.get( position ).getProductImage() )
                    .into( holder.ivNotNewOrderThumbnail );
        }
        final String orderId = "#" + ordersList.get( position ).getId();
        holder.tvNotNewOrderOrderId.setText( orderId );
        holder.tvNotNewOrderProductName.setText( ordersList.get( position ).getProductName() );
        holder.tvNotNewOrderGrandTotal.setText( IndonesianCurrency.format( ordersList.get( position ).getGrandTotal() ) );

        holder.btNotNewOrderDetail.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent( context, OrderDetailsActivity.class );
                intent.putExtra( "orderId", ordersList.get( position ).getId() + "" );
                intent.putExtra( "productName", ordersList.get( position ).getProductName() );
                intent.putExtra( "sellerName", ordersList.get( position ).getSellerName() );
                intent.putExtra( "buyerName", ordersList.get( position ).getBuyerName() );
                intent.putExtra( "description", ordersList.get( position ).getDescription() );
                intent.putExtra( "count", ordersList.get( position ).getCount() + "" );
                intent.putExtra( "totalWeight", ordersList.get( position ).getTotalWeight() + " g" );
                intent.putExtra( "totalPrice", IndonesianCurrency.format( ordersList.get( position ).getTotalPrice() ) );
                intent.putExtra( "totalShippingCost", IndonesianCurrency.format( ordersList.get( position ).getShippingCost() ) );
                intent.putExtra( "grandTotal", IndonesianCurrency.format( ordersList.get( position ).getGrandTotal() ) );
                intent.putExtra( "shippingAgents", ordersList.get( position ).getShippingAgents() );
                intent.putExtra( "shippingServiceCode", ordersList.get( position ).getShippingServiceCode() );
                intent.putExtra( "shippingName", ordersList.get( position ).getShippingName() );
                intent.putExtra( "shippingPhone", ordersList.get( position ).getShippingPhone() );
                intent.putExtra( "shippingProvince",  ordersList.get( position ).getShippingProvinceName() );
                intent.putExtra( "shippingCity",  ordersList.get( position ).getShippingCityName() );
                intent.putExtra( "shippingSubdistrict",  ordersList.get( position ).getShippingSubdistrictName() );
                intent.putExtra( "shippingAddress", ordersList.get( position ).getShippingAddress() );
                intent.putExtra( "paymentReceipt", ordersList.get( position ).getPaymentImage() );
                context.startActivity( intent );

            }
        } );
    }

    @Override
    public int getItemCount() {
        return ordersList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ConstraintLayout clNotificationNewOrder;
        public ImageView ivNotNewOrderThumbnail;
        public TextView tvNotNewOrderOrderId;
        public TextView tvNotNewOrderProductName;
        public TextView tvNotNewOrderGrandTotal;
        public Button btNotNewOrderDetail;

        public ViewHolder(View itemView) {
            super( itemView );

            clNotificationNewOrder  = (ConstraintLayout) itemView.findViewById( R.id.clNotificationNewOrder );
            ivNotNewOrderThumbnail  = (ImageView) itemView.findViewById( R.id.ivNotNewOrderThumbnail );
            tvNotNewOrderOrderId    = (TextView) itemView.findViewById( R.id.tvNotNewOrderOrderId );
            tvNotNewOrderProductName= (TextView) itemView.findViewById( R.id.tvNotNewOrderProductName );
            tvNotNewOrderGrandTotal = (TextView) itemView.findViewById( R.id.tvNotNewOrderGrandTotal );
            btNotNewOrderDetail     = (Button) itemView.findViewById( R.id.btNotNewOrderDetailButton );
        }
    }

}
