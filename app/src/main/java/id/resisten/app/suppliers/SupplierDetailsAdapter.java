package id.resisten.app.suppliers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;

import id.resisten.app.R;
import id.resisten.app.retrofit.APIUtils;

/**
 * Created by LEKTOP on 16-Apr-17.
 */

public class SupplierDetailsAdapter extends RecyclerView.Adapter<SupplierDetailsAdapter.ViewHolder> {

    private Context context;
    private List<SupplierDetailsModel> products;

    public SupplierDetailsAdapter(Context context, List<SupplierDetailsModel> products) {
        this.context = context;
        this.products = products;
    }

    @Override
    public SupplierDetailsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from( parent.getContext() ).inflate( R.layout.activity_supplier_details_card, parent, false );
        return new ViewHolder( itemView );
    }

    @Override
    public void onBindViewHolder(SupplierDetailsAdapter.ViewHolder holder, int position) {

        DecimalFormat IndonesianCurrency = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol( "Rp. " );
        formatRp.setMonetaryDecimalSeparator( ',' );
        formatRp.setGroupingSeparator( '.' );

        IndonesianCurrency.setDecimalFormatSymbols( formatRp );

        Glide.with( context ).load( APIUtils.BASE_URL + "/show_image/" + products.get( position ).getImage() )
                .into( holder.productThumb );

        holder.productName.setText( products.get( position ).getName() );
        holder.productPrice.setText( IndonesianCurrency.format(products.get( position ).getPrice() ) );
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView productThumb;
        public TextView productName;
        public TextView productPrice;

        public ViewHolder(View itemView) {
            super( itemView );

            productThumb    = (ImageView) itemView.findViewById( R.id.supplierProductsThumb );
            productName     = (TextView) itemView.findViewById( R.id.supplierProductsName );
            productPrice     = (TextView) itemView.findViewById( R.id.supplierProductsPrice );
        }
    }
}
