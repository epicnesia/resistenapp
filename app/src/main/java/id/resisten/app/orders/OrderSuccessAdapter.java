package id.resisten.app.orders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import id.resisten.app.R;
import id.resisten.app.settings.BanksAdapter;
import id.resisten.app.settings.BanksModel;

/**
 * Created by Top on 23-May-17.
 */

public class OrderSuccessAdapter extends RecyclerView.Adapter<OrderSuccessAdapter.ViewHolder> {

    private Context context;
    private List<BanksModel> banksList;

    public OrderSuccessAdapter(Context context, List<BanksModel> banksList) {
        this.context = context;
        this.banksList = banksList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from( parent.getContext() ).inflate( R.layout.activity_order_success_bank_rv, parent, false );
        return new OrderSuccessAdapter.ViewHolder( itemView );
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvOrderBankName.setText(banksList.get(position).getBankName());
        holder.tvOrderAccountNumber.setText(banksList.get(position).getAccountNumber());
        holder.tvOrderAccountName.setText(banksList.get(position).getAccountName());
    }

    @Override
    public int getItemCount() {
        return banksList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvOrderBankName, tvOrderAccountNumber, tvOrderAccountName;

        public ViewHolder(View itemView) {
            super(itemView);

            tvOrderBankName     = (TextView) itemView.findViewById(R.id.tvOrderBankName);
            tvOrderAccountNumber= (TextView) itemView.findViewById(R.id.tvOrderAccountNumber);
            tvOrderAccountName  = (TextView) itemView.findViewById(R.id.tvOrderAccountName);
        }
    }
}
