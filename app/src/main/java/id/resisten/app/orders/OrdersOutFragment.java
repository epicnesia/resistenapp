package id.resisten.app.orders;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import id.resisten.app.R;
import id.resisten.app.retrofit.APIServices;
import id.resisten.app.retrofit.APIUtils;
import id.resisten.app.users.LoginActivity;
import id.resisten.app.users.SessionManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class OrdersOutFragment extends Fragment {

    // Session Manager class
    private SessionManager session;
    private HashMap<String, String> user;

    private int page;

    private ProgressBar progressBar;

    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private OrderAdapter orderAdapter;
    private List<OrdersDataModel> ordersList;

    public OrdersOutFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );

        // Session manager instance
        session = new SessionManager(getActivity().getApplicationContext());
        if(!session.isLoggedIn()){
            Intent loginIntent = new Intent(this.getActivity(), LoginActivity.class);
            this.getActivity().startActivity(loginIntent);
            getActivity().finish();
        }

        user = session.getUserDetails();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate( R.layout.fragment_orders_out, container, false );
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        if (user.get(SessionManager.KEY_TYPE).equals( "2" )) {

            progressBar = (ProgressBar) view.findViewById( R.id.pbOrderList );

            page = 1;

            recyclerView = (RecyclerView) view.findViewById( R.id.rvOrderList );
            recyclerView.setHasFixedSize( true );
            ordersList = new ArrayList<>();

            load_data( page );

            layoutManager = new LinearLayoutManager( getActivity() );
            recyclerView.setLayoutManager( layoutManager );

            orderAdapter = new OrderAdapter( getActivity(), ordersList );
            recyclerView.setAdapter( orderAdapter );

            recyclerView.addOnScrollListener( new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    if (layoutManager.findLastCompletelyVisibleItemPosition() == ordersList.size() - 1) {
                        load_data( ++page );
                    }
                }
            } );

        }

    }

    private void load_data(final int page) {

        AsyncTask<Integer, Void, Void> task = new AsyncTask<Integer, Void, Void>() {
            @Override
            protected Void doInBackground(Integer... params) {

                APIServices apiServices     = APIUtils.getAPIService();
                Call<ResponseBody> result   = apiServices.getOrders( user.get(SessionManager.KEY_TOKEN), "out", page );
                try {

                    Response<ResponseBody> response = result.execute();

                    JSONObject jsonResponse = new JSONObject( response.body().string() );
                    boolean status          = jsonResponse.getBoolean( "status" );

                    if(status){

                        JSONObject message = jsonResponse.getJSONObject( "message" );

                        JSONArray array = message.getJSONArray( "data" );
                        for(int i=0;i<array.length();i++){

                            JSONObject object       = array.getJSONObject( i );
                            JSONObject objectSeller = object.getJSONObject( "seller" );
                            JSONObject objectBuyer  = object.getJSONObject( "buyer" );
                            JSONObject objectProduct= object.getJSONObject( "product" );
                            JSONObject objectCourier= object.getJSONObject( "courier" );

                            OrdersDataModel data = new OrdersDataModel(
                                    object.getInt( "id" ),
                                    object.getInt( "product_id" ),
                                    object.getInt( "seller_id" ),
                                    object.getInt( "buyer_id" ),
                                    object.getInt( "count" ),
                                    object.getInt( "total_weight" ),
                                    object.getInt( "total_price" ),
                                    object.getString( "description" ),
                                    object.getInt( "shipping_province_id" ),
                                    object.getString( "shipping_province_name" ),
                                    object.getInt( "shipping_city_id" ),
                                    object.getString( "shipping_city_name" ),
                                    object.getInt( "shipping_subdistrict_id" ),
                                    object.getString( "shipping_subdistrict_name" ),
                                    object.getInt( "shipping_cost" ),
                                    object.getString( "shipping_name" ),
                                    object.getString( "shipping_phone" ),
                                    object.getString( "shipping_address" ),
                                    object.getString( "shipping_date" ),
                                    objectCourier.getString( "name" ),
                                    object.getString( "shipping_service_code" ),
                                    object.getString( "shipping_code" ),
                                    object.getString( "shipping_status" ),
                                    object.getInt( "grand_total" ),
                                    object.getString( "payment_image" ),
                                    object.getString( "created_at" ),
                                    object.getString( "updated_at" ),
                                    objectSeller.getString( "name" ),
                                    objectBuyer.getString( "name" ),
                                    objectProduct.getString( "name" ),
                                    objectProduct.getString( "image" )
                            );

                            ordersList.add( data );

                        }

                    } else {
                        String message = jsonResponse.getString( "message" );

                        AlertDialog.Builder builder = new AlertDialog.Builder( getActivity() );
                        builder.setMessage( message )
                                .setNegativeButton( "Retry", null )
                                .create()
                                .show();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return null;

            }

            @Override
            protected void onPostExecute(Void aVoid){
                progressBar.setVisibility( View.GONE );
                orderAdapter.notifyDataSetChanged();
            }
        };

        task.execute();

    }

}
