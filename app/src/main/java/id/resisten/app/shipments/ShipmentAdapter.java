package id.resisten.app.shipments;

import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;

import id.resisten.app.R;
import id.resisten.app.orders.OrdersDataModel;
import id.resisten.app.retrofit.APIUtils;

/**
 * Created by LEKTOP on 16-May-17.
 */

public class ShipmentAdapter extends RecyclerView.Adapter<ShipmentAdapter.ViewHolder> {

    private Context context;
    private List<OrdersDataModel> ordersList;

    private DecimalFormat IndonesianCurrency;
    private DecimalFormatSymbols formatRp;

    public ShipmentAdapter(Context context, List<OrdersDataModel> ordersList) {
        this.context = context;
        this.ordersList = ordersList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from( parent.getContext() ).inflate( R.layout.fragment_shipment_rv, parent, false );
        return new ShipmentAdapter.ViewHolder( itemView );
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        IndonesianCurrency = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol( "Rp. " );
        formatRp.setMonetaryDecimalSeparator( ',' );
        formatRp.setGroupingSeparator( '.' );

        IndonesianCurrency.setDecimalFormatSymbols( formatRp );

        if(!ordersList.get( position ).getProductImage().equals( "null" )){
            Glide.with( context ).load( APIUtils.BASE_URL + "/show_image/" + ordersList.get( position ).getProductImage() )
                    .into( holder.ivShipmentThumbnail );
        }
        final String orderId = "#" + ordersList.get( position ).getId();
        holder.tvShipmentOrderId.setText( orderId );
        holder.tvShipmentProductName.setText( ordersList.get( position ).getProductName() );
        holder.tvShipmentBuyerName.setText( ordersList.get( position ).getBuyerName() );

        holder.btShipmentDetailButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( context, ShipmentDetailsActivity.class );
                intent.putExtra( "orderId", ordersList.get( position ).getId() + "" );
                intent.putExtra( "productName", ordersList.get( position ).getProductName() );
                intent.putExtra( "sellerName", ordersList.get( position ).getSellerName() );
                intent.putExtra( "buyerName", ordersList.get( position ).getBuyerName() );
                intent.putExtra( "description", ordersList.get( position ).getDescription() );
                intent.putExtra( "count", ordersList.get( position ).getCount() + "" );
                intent.putExtra( "totalWeight", ordersList.get( position ).getTotalWeight() + " g" );
                intent.putExtra( "totalPrice", IndonesianCurrency.format( ordersList.get( position ).getTotalPrice() ) );
                intent.putExtra( "totalShippingCost", IndonesianCurrency.format( ordersList.get( position ).getShippingCost() ) );
                intent.putExtra( "grandTotal", IndonesianCurrency.format( ordersList.get( position ).getGrandTotal() ) );
                intent.putExtra( "shippingAgents", ordersList.get( position ).getShippingAgents() );
                intent.putExtra( "shippingServiceCode", ordersList.get( position ).getShippingServiceCode() );
                intent.putExtra( "shippingName", ordersList.get( position ).getShippingName() );
                intent.putExtra( "shippingPhone", ordersList.get( position ).getShippingPhone() );
                intent.putExtra( "shippingProvince",  ordersList.get( position ).getShippingProvinceName() );
                intent.putExtra( "shippingCity",  ordersList.get( position ).getShippingCityName() );
                intent.putExtra( "shippingSubdistrict",  ordersList.get( position ).getShippingSubdistrictName() );
                intent.putExtra( "shippingAddress", ordersList.get( position ).getShippingAddress() );
                intent.putExtra( "paymentReceipt", ordersList.get( position ).getPaymentImage() );
                context.startActivity( intent );
            }
        } );

    }

    @Override
    public int getItemCount() {
        return ordersList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public ConstraintLayout clShipment;
        public ImageView ivShipmentThumbnail;
        public TextView tvShipmentOrderId;
        public TextView tvShipmentProductName;
        public TextView tvShipmentBuyerName;
        public Button btShipmentDetailButton;

        public ViewHolder(View itemView) {
            super( itemView );

            clShipment              = (ConstraintLayout) itemView.findViewById( R.id.clShipment );
            ivShipmentThumbnail     = (ImageView) itemView.findViewById( R.id.ivShipmentThumbnail );
            tvShipmentOrderId       = (TextView) itemView.findViewById( R.id.tvShipmentOrderId );
            tvShipmentProductName   = (TextView) itemView.findViewById( R.id.tvShipmentProductName );
            tvShipmentBuyerName     = (TextView) itemView.findViewById( R.id.tvShipmentBuyerName );
            btShipmentDetailButton  = (Button) itemView.findViewById( R.id.btShipmentDetailButton );

        }
    }
}
