package id.resisten.app.suppliers;

/**
 * Created by LEKTOP on 19-Apr-17.
 */

public class SuppliersModel {

    private int id, relationStatus;
    private String name, storeName, email, phone, image;

    public SuppliersModel(int id, int relationStatus, String name, String storeName, String email, String phone, String image) {
        this.id = id;
        this.relationStatus = relationStatus;
        this.name = name;
        this.storeName = storeName;
        this.email = email;
        this.phone = phone;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRelationStatus() {
        return relationStatus;
    }

    public void setRelationStatus(int relationStatus) {
        this.relationStatus = relationStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
