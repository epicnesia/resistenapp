package id.resisten.app.suppliers;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import id.resisten.app.R;
import id.resisten.app.retrofit.APIUtils;
import id.resisten.app.users.CircleTransform;

/**
 * Created by LEKTOP on 14-Apr-17.
 */

public class SupplierListAdapter extends RecyclerView.Adapter<SupplierListAdapter.ViewHolder> {

    private Context context;
    private List<SupplierListModel> supplierList;

    public SupplierListAdapter(Context context, List<SupplierListModel> supplierList) {
        this.context = context;
        this.supplierList = supplierList;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from( parent.getContext() ).inflate( R.layout.activity_add_supplier_rv, parent, false );
        return new ViewHolder( itemView );
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        if(!supplierList.get( position ).getImage().equals( "null" )) {
            Glide.with( context ).load( APIUtils.BASE_URL + "/show_image/" + supplierList.get( position ).getImage() )
                    .crossFade()
                    .thumbnail( 0.5f )
                    .bitmapTransform( new CircleTransform( context ) )
                    .diskCacheStrategy( DiskCacheStrategy.ALL )
                    .into( holder.propict );
        }

        holder.userName.setText( supplierList.get( position ).getName() );

        if(supplierList.get( position ).getStoreName() != "null") {
            holder.userStoreName.setText( supplierList.get( position ).getStoreName() );
        } else {
            holder.userStoreName.setText( "-" );
        }

        holder.relativeLayout.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = supplierList.get( position ).getId();

                Intent intent = new Intent(context, SupplierDetailsActivity.class);
                intent.putExtra( "supplier_id", id + "" );
                intent.putExtra( "related", "0" );
                context.startActivity( intent );

            }
        } );

    }

    @Override
    public int getItemCount() {
        return supplierList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout relativeLayout;
        public ImageView propict;
        public TextView userName;
        public TextView userStoreName;

        public ViewHolder(View itemView) {
            super( itemView );

            relativeLayout = (RelativeLayout) itemView.findViewById( R.id.rvSupplierList );
            propict = (ImageView) itemView.findViewById( R.id.ivUserPropict );
            userName = (TextView) itemView.findViewById( R.id.tvUserName );
            userStoreName = (TextView) itemView.findViewById( R.id.tvUserStore );
        }
    }
}
