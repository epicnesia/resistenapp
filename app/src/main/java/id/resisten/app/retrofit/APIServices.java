package id.resisten.app.retrofit;

import java.util.HashMap;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by LEKTOP on 04-Apr-17.
 */

public interface APIServices {

    // Register process
    @POST("/api/register")
    @FormUrlEncoded
    Call<ResponseBody> register(@FieldMap HashMap<String, String> params);

    // Login process
    @POST("/api/login")
    @FormUrlEncoded
    Call<ResponseBody> login(@FieldMap HashMap<String, String> params);

    // Update user profile data
    @POST("/api/users/update")
    @Multipart
    Call<ResponseBody> updateProfile(@PartMap() Map<String, RequestBody> partMap,
                                     @Part MultipartBody.Part file);

    // Post products
    @POST("/api/products")
    @Multipart
    Call<ResponseBody> postProduct( @PartMap() Map<String, RequestBody> partMap,
                                    @Part MultipartBody.Part file);

    // Get new products list
    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET("/api/products")
    Call<ResponseBody> getNewProducts(@Query("api_token") String api_token, @Query( "page" ) int page);

    // Search suppliers list
    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET("/api/relations/search")
    Call<ResponseBody> searchSuppliersList(@Query("api_token") String api_token, @Query( "s" ) String s);

    // Get suppliers list
    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET("/api/relations/supllier_list")
    Call<ResponseBody> supplierList(@Query("api_token") String api_token, @Query( "page" ) int page);

    // Get supplier details
    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET("/api/relations/{id}")
    Call<ResponseBody> getUserRelationsDetails(@Path( "id" ) int id, @QueryMap Map<String, String> query);

    // Search cooperation request
    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET("/api/relations/get_coop_request")
    Call<ResponseBody> getCoopRequest(@Query("api_token") String api_token, @Query( "page" ) int page);

    // Add supplier
    @POST("/api/relations/add_relations")
    @FormUrlEncoded
    Call<ResponseBody> addSupplier(@FieldMap HashMap<String, String> params);

    // Accept relation request
    @POST("/api/relations/accept_relations")
    @FormUrlEncoded
    Call<ResponseBody> acceptRelations(@FieldMap HashMap<String, String> params);

    // Reject relation request
    @POST("/api/relations/reject_relations")
    @FormUrlEncoded
    Call<ResponseBody> rejectRelations(@FieldMap HashMap<String, String> params);

    // Get reseller list
    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET("/api/relations/reseller_list")
    Call<ResponseBody> resellerList(@Query("api_token") String api_token, @Query( "page" ) int page);

    // Load user's target
    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET("/api/users/load_target")
    Call<ResponseBody> loadTarget(@Query("api_token") String api_token);

    // Load shipping agents
    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET("/api/shipping_agents")
    Call<ResponseBody> getShippingAgents(@Query("api_token") String api_token);

    // Post new order
    @POST("/api/orders")
    @FormUrlEncoded
    Call<ResponseBody> postOrder(@FieldMap HashMap<String, String> params);

    // Get orders
    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET("/api/orders")
    Call<ResponseBody> getOrders(@Query("api_token") String api_token, @Query("need") String need, @Query("page") int page);

    // Post payment image
    @POST("/api/orders/payment")
    @Multipart
    Call<ResponseBody> postPaymentImage( @PartMap() Map<String, RequestBody> partMap,
                                    @Part MultipartBody.Part file);

    // Update order's status to packed
    @POST("/api/orders/pack")
    @FormUrlEncoded
    Call<ResponseBody> packOrder(@Field("api_token") String api_token, @Field("order_id") int orderId);

    // Update order's status to packed
    @POST("/api/orders/shipping_code")
    @FormUrlEncoded
    Call<ResponseBody> submitShippingCode(@Field("api_token") String api_token, @Field("order_id") int orderId,
                                          @Field("shipping_code") String shippingCode);

    // Get banks
    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET("/api/users/get_bank")
    Call<ResponseBody> getBanks(@Query("api_token") String api_token, @Query("page") int page);

    // Get seller banks
    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @GET("/api/users/get_seller_bank")
    Call<ResponseBody> getSellerBanks(@Query("api_token") String api_token, @Query("page") int page, @Query("seller_id") int sellerId);

    // Add bank
    @POST("/api/users/add_bank")
    @FormUrlEncoded
    Call<ResponseBody> addBank(@FieldMap HashMap<String, String> params);

    // Add bank
    @Headers({"Accept: application/json", "Content-Type: application/json"})
    @DELETE("/api/users/del_bank")
    Call<ResponseBody> delBank(@Query("api_token") String api_token, @Query("bank_id") int bankId);

    // Add bank
    @POST("/api/orders/update_shipping_status")
    @FormUrlEncoded
    Call<ResponseBody> updateOrderStatus(@FieldMap HashMap<String, String> params);
}
