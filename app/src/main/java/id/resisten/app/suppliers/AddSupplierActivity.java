package id.resisten.app.suppliers;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.ShareCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import id.resisten.app.R;
import id.resisten.app.retrofit.APIServices;
import id.resisten.app.retrofit.APIUtils;
import id.resisten.app.users.LoginActivity;
import id.resisten.app.users.SessionManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddSupplierActivity extends AppCompatActivity {

    // Session Manager class
    private SessionManager session;
    private HashMap<String, String> user;

    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private SupplierListAdapter supplierListAdapter;
    private List<SupplierListModel> supplierList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_add_supplier );

        // Session manager instance
        session = new SessionManager(AddSupplierActivity.this);
        if(!session.isLoggedIn()){
            Intent loginIntent = new Intent(AddSupplierActivity.this, LoginActivity.class);
            startActivity(loginIntent);
            finish();
        }

        user = session.getUserDetails();

        getSupportActionBar().setTitle( getResources().getString( R.string.addSupplierTitle ) );
        getSupportActionBar().setDisplayHomeAsUpEnabled( true );

        recyclerView    = (RecyclerView) findViewById( R.id.rvAddSupplierList );
        recyclerView.setHasFixedSize( true );
        supplierList    = new ArrayList<>();

        final ImageView btSearchSupplier = (ImageView) findViewById( R.id.btSearchSupplier );
        final EditText etSearchSupplier = (EditText) findViewById( R.id.etSearchSupplier );

        btSearchSupplier.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                supplierList.clear();

                final ProgressDialog spinner = new ProgressDialog( AddSupplierActivity.this );
                spinner.setMessage( "Searching..." );
                spinner.setCancelable( false );
                spinner.show();

                final String searchParam = etSearchSupplier.getText().toString();

                APIServices apiServices = APIUtils.getAPIService();
                Call<ResponseBody> result = apiServices.searchSuppliersList( user.get( SessionManager.KEY_TOKEN ), searchParam );
                result.enqueue( new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        spinner.dismiss();

                        try {

                            JSONObject jsonResponse = new JSONObject( response.body().string() );
                            boolean status = jsonResponse.getBoolean( "status" );

                            if(status){

                                JSONArray array = jsonResponse.getJSONArray( "message" );
                                if(array.length() > 0) {
                                    for (int i = 0; i < array.length(); i++) {

                                        JSONObject object = array.getJSONObject( i );

                                        SupplierListModel data = new SupplierListModel(
                                                object.getInt( "id" ),
                                                object.getString( "name" ),
                                                object.getString( "store_name" ),
                                                object.getString( "profile_picture" )
                                        );

                                        supplierList.add( data );

                                    }

                                    layoutManager = new LinearLayoutManager( AddSupplierActivity.this );
                                    recyclerView.setLayoutManager( layoutManager );

                                    supplierListAdapter = new SupplierListAdapter( AddSupplierActivity.this, supplierList );
                                    recyclerView.setAdapter( supplierListAdapter );

                                } else {
                                    AlertDialog.Builder popup = new AlertDialog.Builder( AddSupplierActivity.this );
                                    popup.setMessage( getResources().getString( R.string.noSupplierFoundMessage ) )
                                             .setNegativeButton( "No", null )
                                             .setPositiveButton( "Yes", new DialogInterface.OnClickListener() {
                                                 @Override
                                                 public void onClick(DialogInterface dialog, int which) {

                                                     ShareCompat.IntentBuilder
                                                             .from( AddSupplierActivity.this )
                                                             .setText( getResources().getString( R.string.inviteSupplierText ) )
                                                             .setType( "text/plain" )
                                                             .setChooserTitle( getResources().getString( R.string.shareVia ) )
                                                             .startChooser();

                                                 }
                                             })
                                            .create()
                                            .show();


                                }

                            } else {

                                String message = jsonResponse.getString( "message" );

                                AlertDialog.Builder builder = new AlertDialog.Builder( AddSupplierActivity.this );
                                builder.setMessage( message )
                                        .setNegativeButton( "Retry", null )
                                        .create()
                                        .show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        spinner.dismiss();
                        t.printStackTrace();
                    }
                } );

            }
        } );

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
