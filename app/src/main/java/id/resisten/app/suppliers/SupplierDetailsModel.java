package id.resisten.app.suppliers;

/**
 * Created by LEKTOP on 16-Apr-17.
 */

public class SupplierDetailsModel {

    private int id, price;
    private String name, image;

    public SupplierDetailsModel(int id, int price, String name, String image) {
        this.id = id;
        this.price = price;
        this.name = name;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
