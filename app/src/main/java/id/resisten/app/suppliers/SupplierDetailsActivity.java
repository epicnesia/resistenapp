package id.resisten.app.suppliers;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import id.resisten.app.MainActivity;
import id.resisten.app.R;
import id.resisten.app.retrofit.APIServices;
import id.resisten.app.retrofit.APIUtils;
import id.resisten.app.users.CircleTransform;
import id.resisten.app.users.LoginActivity;
import id.resisten.app.users.SessionManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SupplierDetailsActivity extends AppCompatActivity {

    // Session Manager class
    private SessionManager session;
    private HashMap<String, String> user;

    private int page;

    private RecyclerView recyclerView;
    private GridLayoutManager gridLayoutManager;
    private SupplierDetailsAdapter supplierDetailsAdapter;
    private List<SupplierDetailsModel> productsList;

    private ImageView suppPropict;
    private TextView suppName;
    private TextView suppStore;
    private Button btAddSupplier;

    private int id, related;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_supplier_details );

        // Session manager instance
        session = new SessionManager( SupplierDetailsActivity.this );
        if (!session.isLoggedIn()) {
            Intent loginIntent = new Intent( SupplierDetailsActivity.this, LoginActivity.class );
            SupplierDetailsActivity.this.startActivity( loginIntent );
            SupplierDetailsActivity.this.finish();
        }

        user = session.getUserDetails();

        getSupportActionBar().setTitle( getResources().getString( R.string.supplierDetailTitle ) );
        getSupportActionBar().setDisplayHomeAsUpEnabled( true );

        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            id      = Integer.parseInt( extras.getString( "supplier_id" ) );
            related = Integer.parseInt( extras.getString( "related" ) );

        }

        suppPropict     = (ImageView) findViewById( R.id.ivNewProductSuppPropict);
        suppName        = (TextView) findViewById( R.id.tvSuppName );
        suppStore       = (TextView) findViewById( R.id.tvSuppStore );
        btAddSupplier   = (Button) findViewById( R.id.btAddSupplier );

        page = 1;

        recyclerView    = (RecyclerView) findViewById( R.id.rvSupplierProducts );
        productsList    = new ArrayList<>();

        setDetailsProfile(id);

        setDetailsProducts( id, page );

        gridLayoutManager = new GridLayoutManager( SupplierDetailsActivity.this, 2 );
        recyclerView.setLayoutManager( gridLayoutManager );

        supplierDetailsAdapter = new SupplierDetailsAdapter( SupplierDetailsActivity.this, productsList );
        recyclerView.setAdapter( supplierDetailsAdapter );

        recyclerView.addOnScrollListener( new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if(gridLayoutManager.findLastCompletelyVisibleItemPosition() == productsList.size()-1){
                    setDetailsProducts( id, ++page );
                }
            }
        } );

        if(related == 1){

            btAddSupplier.setText(getResources().getString( R.string.chat ));

        } else if(related == 0){
            btAddSupplier.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    add_supplier();
                }
            } );
        }

    }

    private void setDetailsProfile(final int id) {
        HashMap<String, String> param = new HashMap<>();
        param.put( "api_token", user.get( SessionManager.KEY_TOKEN ) );

        APIServices apiServices = APIUtils.getAPIService();
        Call<ResponseBody> result = apiServices.getUserRelationsDetails( id, param );
        result.enqueue( new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                JSONObject jsonResponse = null;
                try {

                    jsonResponse = new JSONObject( response.body().string() );
                    boolean status = jsonResponse.getBoolean( "status" );

                    if(status){

                        JSONObject message = jsonResponse.getJSONObject( "message" );
                        JSONObject supplier= message.getJSONObject( "supplier" );

                        if(!supplier.getString( "profile_picture" ).equals("null")){
                            Glide.with( SupplierDetailsActivity.this ).load( APIUtils.BASE_URL + "/show_image/" + supplier.getString( "profile_picture" ) )
                                    .crossFade()
                                    .thumbnail( 0.5f )
                                    .bitmapTransform( new CircleTransform( SupplierDetailsActivity.this ) )
                                    .diskCacheStrategy( DiskCacheStrategy.ALL )
                                    .into( suppPropict );
                        }

                        suppName.setText( supplier.getString( "name" ) );
                        if(!supplier.getString( "store_name" ).equals("null")){
                            suppStore.setText( supplier.getString( "store_name" ) );
                        } else {
                            suppStore.setText( "-" );
                        }

                    } else {
                        String message = jsonResponse.getString( "message" );

                        AlertDialog.Builder builder = new AlertDialog.Builder( SupplierDetailsActivity.this );
                        builder.setMessage( message )
                                .setNegativeButton( "Retry", null )
                                .create()
                                .show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
            }
        } );
    }

    // Set all details data including card view of products
    private void setDetailsProducts(final int id, final int page) {

        AsyncTask<Integer, Void, Void> task = new AsyncTask<Integer, Void, Void>() {
            @Override
            protected Void doInBackground(Integer... params) {

                HashMap<String, String> param = new HashMap<>();
                param.put( "api_token", user.get( SessionManager.KEY_TOKEN ) );
                param.put( "page", Integer.toString( page ) );

                APIServices apiServices = APIUtils.getAPIService();
                Call<ResponseBody> result = apiServices.getUserRelationsDetails( id, param );
                try {

                    Response<ResponseBody> response = result.execute();

                    JSONObject jsonResponse = new JSONObject( response.body().string() );
                    boolean status = jsonResponse.getBoolean( "status" );

                    if(status){

                        JSONObject message = jsonResponse.getJSONObject( "message" );
                        JSONObject products= message.getJSONObject( "products" );

                        JSONArray array = products.getJSONArray( "data" );
                        for(int i=0; i < array.length(); i++){

                            JSONObject object = array.getJSONObject( i );

                            SupplierDetailsModel data = new SupplierDetailsModel(
                                    object.getInt( "id" ),
                                    object.getInt( "price" ),
                                    object.getString( "name" ),
                                    object.getString( "image" )
                            );

                            productsList.add( data );

                        }

                    } else {
                        String message = jsonResponse.getString( "message" );

                        AlertDialog.Builder builder = new AlertDialog.Builder( SupplierDetailsActivity.this );
                        builder.setMessage( message )
                                .setNegativeButton( "Retry", null )
                                .create()
                                .show();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid){
                supplierDetailsAdapter.notifyDataSetChanged();
            }
        };

        task.execute();

    }

    private void add_supplier() {
        final ProgressDialog spinner = new ProgressDialog( SupplierDetailsActivity.this );
        spinner.setMessage( getResources().getString( R.string.pleaseWait ) );
        spinner.setCancelable( false );
        spinner.show();

        HashMap<String, String> param = new HashMap<>();
        param.put( "api_token", user.get( SessionManager.KEY_TOKEN ) );
        param.put( "supplier", id + "" );

        APIServices apiServices = APIUtils.getAPIService();
        Call<ResponseBody> result = apiServices.addSupplier( param );
        result.enqueue( new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {

                    spinner.dismiss();

                    JSONObject jsonResponse = new JSONObject( response.body().string() );
                    boolean status = jsonResponse.getBoolean( "status" );
                    String message = jsonResponse.getString( "message" );

                    if(status){

                        Toast.makeText(SupplierDetailsActivity.this, message, Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(SupplierDetailsActivity.this, MainActivity.class);
                        intent.putExtra( "navItemIndex", "7" );
                        intent.putExtra( "CURRENT_TAG", "supplier" );
                        intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
                        startActivity( intent );
                        finish();

                    } else {

                        AlertDialog.Builder builder = new AlertDialog.Builder( SupplierDetailsActivity.this );
                        builder.setMessage( message )
                                .setNegativeButton( "Retry", null )
                                .create()
                                .show();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                spinner.dismiss();
                t.printStackTrace();
            }
        } );
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
