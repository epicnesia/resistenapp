package id.resisten.app.payments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import id.resisten.app.MainActivity;
import id.resisten.app.R;
import id.resisten.app.orders.OrderSuccessActivity;
import id.resisten.app.orders.OrderSuccessAdapter;
import id.resisten.app.retrofit.APIServices;
import id.resisten.app.retrofit.APIUtils;
import id.resisten.app.settings.BanksModel;
import id.resisten.app.utilities.FileUtils;
import id.resisten.app.utilities.ScalingUtilities;
import id.resisten.app.users.LoginActivity;
import id.resisten.app.users.SessionManager;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConfirmPaymentDetailActivity extends AppCompatActivity {

    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;
    private static final int REQUEST_PERMISSION_SETTING = 101;
    private boolean sentToSettings = false;
    private SharedPreferences permissionStatus;

    // Session Manager class
    private SessionManager session;
    private HashMap<String, String> user;

    private String orderId,
            productName,
            sellerName,
            sellerId,
            count,
            totalWeight,
            totalPrice,
            totalShippingCost,
            grandTotal;

    private TextView tvPaymentOrderId,
            tvPaymentProductName,
            tvPaymentSellerName,
            tvPaymentCount,
            tvPaymentTotalWeight,
            tvPaymentTotalPrice,
            tvPaymentTotalShippingCost,
            tvPaymentGrandTotal;

    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private OrderSuccessAdapter orderSuccessAdapter;
    private List<BanksModel> banksList;

    private int page;

    private ProgressBar progressBar;

    private ImageView ivPaymentImage;

    private Button btConfirm;

    private Bitmap bitmap;

    private int PICK_IMAGE_REQUEST = 1;

    private Uri filePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_confirm_payment_detail );

        permissionStatus = getSharedPreferences("permissionStatus", MODE_PRIVATE);

        // Session manager instance
        session = new SessionManager(ConfirmPaymentDetailActivity.this);
        if(!session.isLoggedIn()){
            Intent loginIntent = new Intent(ConfirmPaymentDetailActivity.this, LoginActivity.class);
            startActivity(loginIntent);
            finish();
        }

        user = session.getUserDetails();

        getSupportActionBar().setTitle( getResources().getString( R.string.orderDetail ) );
        getSupportActionBar().setDisplayHomeAsUpEnabled( true );

        // UI initializing
        tvPaymentOrderId        = (TextView) findViewById( R.id.tvPaymentOrderId );
        tvPaymentProductName    = (TextView) findViewById( R.id.tvPaymentProductName );
        tvPaymentSellerName     = (TextView) findViewById( R.id.tvPaymentSellerName );
        tvPaymentCount          = (TextView) findViewById( R.id.tvPaymentOrderCount );
        tvPaymentTotalWeight    = (TextView) findViewById( R.id.tvPaymentTotalWeight );
        tvPaymentTotalPrice     = (TextView) findViewById( R.id.tvPaymentOrderTotalPrice );
        tvPaymentTotalShippingCost = (TextView) findViewById( R.id.tvPaymentOrderShippingCost );
        tvPaymentGrandTotal     = (TextView) findViewById( R.id.tvPaymentOrderGrandTotal );

        ivPaymentImage          = (ImageView) findViewById( R.id.ivPaymentImage );

        btConfirm               = (Button) findViewById( R.id.btConfirm );

        // Get intent's extras
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            orderId             = extras.getString( "orderId" );
            sellerId            = extras.getString( "sellerId" );
            productName         = extras.getString( "productName" );
            sellerName          = extras.getString( "sellerName" );
            count               = extras.getString( "count" );
            totalWeight         = extras.getString( "totalWeight" );
            totalPrice          = extras.getString( "totalPrice" );
            totalShippingCost   = extras.getString( "totalShippingCost" );
            grandTotal          = extras.getString( "grandTotal" );
        }

        // Setup preview
        tvPaymentOrderId.setText( orderId );
        tvPaymentProductName.setText( productName );
        tvPaymentSellerName.setText( sellerName );
        tvPaymentCount.setText( count );
        tvPaymentTotalWeight.setText( totalWeight );
        tvPaymentTotalPrice.setText( totalPrice );
        tvPaymentTotalShippingCost.setText( totalShippingCost );
        tvPaymentGrandTotal.setText( grandTotal );

        // Payment details section
        progressBar = (ProgressBar) findViewById( R.id.pbPaymentDetails );

        page = 1;

        recyclerView    = (RecyclerView) findViewById(R.id.rvPaymentDetails);
        recyclerView.setHasFixedSize( true );
        banksList       = new ArrayList<>();

        load_data(page);

        layoutManager   = new LinearLayoutManager( ConfirmPaymentDetailActivity.this );
        recyclerView.setLayoutManager( layoutManager );

        orderSuccessAdapter = new OrderSuccessAdapter(ConfirmPaymentDetailActivity.this, banksList);
        recyclerView.setAdapter(orderSuccessAdapter);

        recyclerView.addOnScrollListener( new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if(layoutManager.findLastCompletelyVisibleItemPosition() == banksList.size()-1){
                    load_data( ++page );
                }
            }
        } );

        // Payment image clicked
        ivPaymentImage.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        } );

        // Confirm button clicked
        btConfirm.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(filePath != null) {

                    final ProgressDialog spinner = new ProgressDialog( ConfirmPaymentDetailActivity.this );
                    spinner.setMessage( getResources().getString( R.string.pleaseWait ) );
                    spinner.setCancelable( false );
                    spinner.show();

                    // create upload service client
                    APIServices apiServices = APIUtils.getAPIService();

                    // create part for file (photo, video, ...)
                    MultipartBody.Part body = prepareFilePart( "image", filePath );

                    // create a map of data to pass along
                    RequestBody order       = createPartFromString( orderId );
                    RequestBody apiToken    = createPartFromString( user.get( SessionManager.KEY_TOKEN ) );

                    HashMap<String, RequestBody> params = new HashMap<>();
                    params.put( "api_token", apiToken );
                    params.put( "order_id", order );

                    // finally, execute the request
                    Call<ResponseBody> result = apiServices.postPaymentImage( params, body );
                    result.enqueue( new Callback<ResponseBody>() {

                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                            spinner.dismiss();
                            
                            try {

                                JSONObject jsonResponse = new JSONObject( response.body().string() );
                                boolean status = jsonResponse.getBoolean( "status" );
                                String message = jsonResponse.getString( "message" );

                                Toast.makeText(ConfirmPaymentDetailActivity.this, message, Toast.LENGTH_LONG).show();

                                Intent intent = new Intent(ConfirmPaymentDetailActivity.this, MainActivity.class);
                                intent.putExtra( "navItemIndex", "10" );
                                intent.putExtra( "CURRENT_TAG", "confirmPayment" );
                                intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
                                startActivity( intent );
                                finish();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            spinner.dismiss();
                            t.printStackTrace();
                        }

                    } );

                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder( ConfirmPaymentDetailActivity.this );
                    builder.setMessage("Please select an image first..!")
                            .setNegativeButton( "Retry", null )
                            .create()
                            .show();
                }

            }
        } );

    }

    private void load_data(final int page) {

        AsyncTask<Integer, Void, Void> task = new AsyncTask<Integer, Void, Void>() {
            @Override
            protected Void doInBackground(Integer... params) {

                APIServices apiServices = APIUtils.getAPIService();
                Call<ResponseBody> result = apiServices.getSellerBanks( user.get( SessionManager.KEY_TOKEN), page, Integer.parseInt(sellerId) );
                try {

                    Response<ResponseBody> response = result.execute();

                    JSONObject jsonResponse = new JSONObject( response.body().string() );
                    boolean status = jsonResponse.getBoolean( "status" );

                    if(status){

                        JSONObject message = jsonResponse.getJSONObject( "message" );

                        JSONArray array = message.getJSONArray( "data" );
                        for(int i=0;i<array.length();i++){

                            JSONObject object = array.getJSONObject( i );

                            BanksModel data = new BanksModel (
                                    object.getInt( "id" ),
                                    object.getString( "bank_name" ),
                                    object.getString( "account_number" ),
                                    object.getString( "account_name" )
                            );

                            banksList.add( data );

                        }

                    } else {

                        String message = jsonResponse.getString( "message" );

                        AlertDialog.Builder builder = new AlertDialog.Builder( ConfirmPaymentDetailActivity.this );
                        builder.setMessage( message )
                                .setNegativeButton( "Retry", null )
                                .create()
                                .show();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid){
                progressBar.setVisibility( View.GONE );
                orderSuccessAdapter.notifyDataSetChanged();
            }
        };

        task.execute();

    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            if (ActivityCompat.checkSelfPermission(ConfirmPaymentDetailActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(ConfirmPaymentDetailActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    //Show Information about why you need the permission
                    AlertDialog.Builder builder = new AlertDialog.Builder(ConfirmPaymentDetailActivity.this);
                    builder.setTitle("Need Storage Permission");
                    builder.setMessage("This app needs storage permission.");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            ActivityCompat.requestPermissions(ConfirmPaymentDetailActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                } else if (permissionStatus.getBoolean(Manifest.permission.READ_EXTERNAL_STORAGE,false)) {
                    //Previously Permission Request was cancelled with 'Dont Ask Again',
                    // Redirect to Settings after showing Information about why you need the permission
                    AlertDialog.Builder builder = new AlertDialog.Builder(ConfirmPaymentDetailActivity.this);
                    builder.setTitle("Need Storage Permission");
                    builder.setMessage("This app needs storage permission.");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            sentToSettings = true;
                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            intent.setData(uri);
                            startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                            Toast.makeText(getBaseContext(), "Go to Permissions to Grant Storage", Toast.LENGTH_LONG).show();
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                } else {
                    //just request the permission
                    ActivityCompat.requestPermissions(ConfirmPaymentDetailActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);
                }


                SharedPreferences.Editor editor = permissionStatus.edit();
                editor.putBoolean(Manifest.permission.READ_EXTERNAL_STORAGE,true);
                editor.commit();

            }

            filePath = data.getData();
            try {
                //Getting the Bitmap from Gallery
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                //Setting the Bitmap to ImageView
                ivPaymentImage.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 120, 120, false));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private RequestBody createPartFromString(String str) {
        return RequestBody.create( MediaType.parse("text/plain"), str );
    }

    private MultipartBody.Part prepareFilePart(String partName, Uri fileUri) {

        Uri newUri = Uri.parse(decodeFile(fileUri.toString(), 1600, 900));

        // use the FileUtils to get the actual file by uri
        File file = FileUtils.getFile( getApplicationContext(), newUri );

        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse( getContentResolver().getType( newUri ) ),
                        file
                );

        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData( partName, file.getName(), requestFile );

    }

    private String decodeFile(String path, int DESIREDWIDTH, int DESIREDHEIGHT) {
        String strMyImagePath = null;
        Bitmap scaledBitmap = null;

        try {
            // Part 1: Decode image
            Bitmap unscaledBitmap = ScalingUtilities.decodeFile(path, DESIREDWIDTH, DESIREDHEIGHT, ScalingUtilities.ScalingLogic.FIT);

            if (!(unscaledBitmap.getWidth() <= DESIREDWIDTH && unscaledBitmap.getHeight() <= DESIREDHEIGHT)) {
                // Part 2: Scale image
                scaledBitmap = ScalingUtilities.createScaledBitmap(unscaledBitmap, DESIREDWIDTH, DESIREDHEIGHT, ScalingUtilities.ScalingLogic.FIT);
            } else {
                unscaledBitmap.recycle();
                return path;
            }

            // Store to tmp file

            String extr = Environment.getExternalStorageDirectory().toString();
            File mFolder = new File(extr + "/TMMFOLDER");
            if (!mFolder.exists()) {
                mFolder.mkdir();
            }

            String s = "tmp.png";

            File f = new File(mFolder.getAbsolutePath(), s);

            strMyImagePath = f.getAbsolutePath();
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(f);
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 75, fos);
                fos.flush();
                fos.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            scaledBitmap.recycle();
        } catch (Throwable e) {
        }

        if (strMyImagePath == null) {
            return path;
        }
        return strMyImagePath;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
