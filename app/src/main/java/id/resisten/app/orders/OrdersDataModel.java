package id.resisten.app.orders;

/**
 * Created by Top on 09-May-17.
 */

public class OrdersDataModel {

    private int id, productId, sellerId, buyerId, count, totalWeight, totalPrice;
    private String description;
    private int shippingProvinceId;
    private String shippingProvinceName;
    private int shippingCityId;
    private String shippingCityName;
    private int shippingSubdistrictId;
    private String shippingSubdistrictName;
    private int shippingCost;
    private String shippingName, shippingPhone, shippingAddress, shippingDate, shippingAgents, shippingServiceCode, shippingCode, shippingStatus;
    private int grandTotal;
    private String paymentImage, createdDate, updatedDate, sellerName, buyerName, productName, productImage;

    public OrdersDataModel(int id, int productId, int sellerId, int buyerId, int count, int totalWeight, int totalPrice, String description, int shippingProvinceId, String shippingProvinceName, int shippingCityId, String shippingCityName, int shippingSubdistrictId, String shippingSubdistrictName, int shippingCost, String shippingName, String shippingPhone, String shippingAddress, String shippingDate, String shippingAgents, String shippingServiceCode, String shippingCode, String shippingStatus, int grandTotal, String paymentImage, String createdDate, String updatedDate, String sellerName, String buyerName, String productName, String productImage) {
        this.id = id;
        this.productId = productId;
        this.sellerId = sellerId;
        this.buyerId = buyerId;
        this.count = count;
        this.totalWeight = totalWeight;
        this.totalPrice = totalPrice;
        this.description = description;
        this.shippingProvinceId = shippingProvinceId;
        this.shippingProvinceName = shippingProvinceName;
        this.shippingCityId = shippingCityId;
        this.shippingCityName = shippingCityName;
        this.shippingSubdistrictId = shippingSubdistrictId;
        this.shippingSubdistrictName = shippingSubdistrictName;
        this.shippingCost = shippingCost;
        this.shippingName = shippingName;
        this.shippingPhone = shippingPhone;
        this.shippingAddress = shippingAddress;
        this.shippingDate = shippingDate;
        this.shippingAgents = shippingAgents;
        this.shippingServiceCode = shippingServiceCode;
        this.shippingCode = shippingCode;
        this.shippingStatus = shippingStatus;
        this.grandTotal = grandTotal;
        this.paymentImage = paymentImage;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
        this.sellerName = sellerName;
        this.buyerName = buyerName;
        this.productName = productName;
        this.productImage = productImage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getSellerId() {
        return sellerId;
    }

    public void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    public int getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(int buyerId) {
        this.buyerId = buyerId;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(int totalWeight) {
        this.totalWeight = totalWeight;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getShippingProvinceId() {
        return shippingProvinceId;
    }

    public void setShippingProvinceId(int shippingProvinceId) {
        this.shippingProvinceId = shippingProvinceId;
    }

    public String getShippingProvinceName() {
        return shippingProvinceName;
    }

    public void setShippingProvinceName(String shippingProvinceName) {
        this.shippingProvinceName = shippingProvinceName;
    }

    public int getShippingCityId() {
        return shippingCityId;
    }

    public void setShippingCityId(int shippingCityId) {
        this.shippingCityId = shippingCityId;
    }

    public String getShippingCityName() {
        return shippingCityName;
    }

    public void setShippingCityName(String shippingCityName) {
        this.shippingCityName = shippingCityName;
    }

    public int getShippingSubdistrictId() {
        return shippingSubdistrictId;
    }

    public void setShippingSubdistrictId(int shippingSubdistrictId) {
        this.shippingSubdistrictId = shippingSubdistrictId;
    }

    public String getShippingSubdistrictName() {
        return shippingSubdistrictName;
    }

    public void setShippingSubdistrictName(String shippingSubdistrictName) {
        this.shippingSubdistrictName = shippingSubdistrictName;
    }

    public int getShippingCost() {
        return shippingCost;
    }

    public void setShippingCost(int shippingCost) {
        this.shippingCost = shippingCost;
    }

    public String getShippingName() {
        return shippingName;
    }

    public void setShippingName(String shippingName) {
        this.shippingName = shippingName;
    }

    public String getShippingPhone() {
        return shippingPhone;
    }

    public void setShippingPhone(String shippingPhone) {
        this.shippingPhone = shippingPhone;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getShippingDate() {
        return shippingDate;
    }

    public void setShippingDate(String shippingDate) {
        this.shippingDate = shippingDate;
    }

    public String getShippingAgents() {
        return shippingAgents;
    }

    public void setShippingAgents(String shippingAgents) {
        this.shippingAgents = shippingAgents;
    }

    public String getShippingServiceCode() {
        return shippingServiceCode;
    }

    public void setShippingServiceCode(String shippingServiceCode) {
        this.shippingServiceCode = shippingServiceCode;
    }

    public String getShippingCode() {
        return shippingCode;
    }

    public void setShippingCode(String shippingCode) {
        this.shippingCode = shippingCode;
    }

    public String getShippingStatus() {
        return shippingStatus;
    }

    public void setShippingStatus(String shippingStatus) {
        this.shippingStatus = shippingStatus;
    }

    public int getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(int grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getPaymentImage() {
        return paymentImage;
    }

    public void setPaymentImage(String paymentImage) {
        this.paymentImage = paymentImage;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }
}
