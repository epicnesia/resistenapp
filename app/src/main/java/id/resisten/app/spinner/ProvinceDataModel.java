package id.resisten.app.spinner;

/**
 * Created by LEKTOP on 23-Apr-17.
 */

public class ProvinceDataModel {

    private int id;
    private String province;

    public ProvinceDataModel(int id, String province) {
        this.id = id;
        this.province = province;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    @Override
    public String toString() {
        return province;
    }
}
