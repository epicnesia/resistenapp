package id.resisten.app.spinner;

/**
 * Created by Top on 02-May-17.
 */

public class CourierServicesDataModel {
    private String service;
    private int cost;

    public CourierServicesDataModel(String service, int cost) {
        this.service = service;
        this.cost = cost;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return service;
    }
}
