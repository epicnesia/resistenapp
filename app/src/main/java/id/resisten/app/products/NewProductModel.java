package id.resisten.app.products;

/**
 * Created by LEKTOP on 11-Apr-17.
 */

public class NewProductModel {

    private int id, supplierId, price, commision, weight, provinceId, cityId, subdistrictId, stock;
    private String supplierName, supplierPropict, createdDate, name, image, description;

    public NewProductModel(int id, int supplierId, int price, int commision, int weight, int provinceId, int cityId, int subdistrictId, int stock, String supplierName, String supplierPropict, String createdDate, String name, String image, String description) {
        this.id = id;
        this.supplierId = supplierId;
        this.price = price;
        this.commision = commision;
        this.weight = weight;
        this.provinceId = provinceId;
        this.cityId = cityId;
        this.subdistrictId = subdistrictId;
        this.stock = stock;
        this.supplierName = supplierName;
        this.supplierPropict = supplierPropict;
        this.createdDate = createdDate;
        this.name = name;
        this.image = image;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(int supplierId) {
        this.supplierId = supplierId;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getCommision() {
        return commision;
    }

    public void setCommision(int commision) {
        this.commision = commision;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(int provinceId) {
        this.provinceId = provinceId;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public int getSubdistrictId() {
        return subdistrictId;
    }

    public void setSubdistrictId(int subdistrictId) {
        this.subdistrictId = subdistrictId;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getSupplierPropict() {
        return supplierPropict;
    }

    public void setSupplierPropict(String supplierPropict) {
        this.supplierPropict = supplierPropict;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
