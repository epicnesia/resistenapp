package id.resisten.app.notifications;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import id.resisten.app.R;
import id.resisten.app.retrofit.APIUtils;

/**
 * Created by Top on 19-Apr-17.
 */

public class CoopRequestAdapter extends RecyclerView.Adapter<CoopRequestAdapter.ViewHolder> {

    private Context context;
    private List<CoopRequestModel> coopRequestList;

    public CoopRequestAdapter(Context context, List<CoopRequestModel> coopRequestList) {
        this.context = context;
        this.coopRequestList = coopRequestList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate( R.layout.fragment_coop_request_rv, parent, false);
        return new ViewHolder( itemView );
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        if(coopRequestList.get( position ).getImage() != "null") {
            Glide.with(context).load(APIUtils.BASE_URL + "/show_image/" + coopRequestList.get(position).getImage())
                    .into(holder.propict);
        }

        holder.coopName.setText(coopRequestList.get( position ).getName());

        holder.relativeLayout.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = coopRequestList.get( position ).getId();
                String name = coopRequestList.get( position ).getName();
                String phone= coopRequestList.get( position ).getPhone();
                String email= coopRequestList.get( position ).getEmail();
                String image= coopRequestList.get( position ).getImage();

                Intent approvalIntent = new Intent( context, ResellerApprovalActivity.class );
                approvalIntent.putExtra( "reseller_id", id + "" );
                approvalIntent.putExtra( "reseller_name", name );
                approvalIntent.putExtra( "reseller_phone", phone );
                approvalIntent.putExtra( "reseller_email", email );
                approvalIntent.putExtra( "reseller_propict", image );
                context.startActivity( approvalIntent );

            }
        } );

    }

    @Override
    public int getItemCount() {
        return coopRequestList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout relativeLayout;
        public ImageView propict;
        public TextView coopName;
        public Button actionButton;

        public ViewHolder(View itemView) {
            super(itemView);

            relativeLayout = (RelativeLayout) itemView.findViewById( R.id.rlCoopRequest );
            propict = (ImageView) itemView.findViewById( R.id.ivCoopRequestPropict );
            coopName= (TextView) itemView.findViewById( R.id.tvCoopRequestName );
            actionButton= (Button) itemView.findViewById( R.id.btActionCoopRequest );
        }
    }
}
