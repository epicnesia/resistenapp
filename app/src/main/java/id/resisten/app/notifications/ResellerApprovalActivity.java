package id.resisten.app.notifications;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import id.resisten.app.MainActivity;
import id.resisten.app.R;
import id.resisten.app.retrofit.APIServices;
import id.resisten.app.retrofit.APIUtils;
import id.resisten.app.users.CircleTransform;
import id.resisten.app.users.LoginActivity;
import id.resisten.app.users.SessionManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResellerApprovalActivity extends AppCompatActivity {

    // Session Manager class
    private SessionManager session;
    private HashMap<String, String> user;

    private int id;

    private ImageView propict;
    private TextView name;
    private TextView phone;
    private TextView email;
    private Button btAccept;
    private Button btReject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_reseller_approval );

        // Session manager instance
        session = new SessionManager(ResellerApprovalActivity.this);
        if(!session.isLoggedIn()){
            Intent loginIntent = new Intent(ResellerApprovalActivity.this, LoginActivity.class);
            startActivity(loginIntent);
            finish();
        }

        user = session.getUserDetails();

        getSupportActionBar().setTitle( getResources().getString( R.string.approvalPage ) );
        getSupportActionBar().setDisplayHomeAsUpEnabled( true );

        name    = (TextView) findViewById( R.id.tvResellerName );
        phone   = (TextView) findViewById( R.id.tvResellerPhone );
        email   = (TextView) findViewById( R.id.tvResellerEmail );
        propict = (ImageView) findViewById( R.id.ivResellerPropict );
        btAccept= (Button) findViewById( R.id.btAccept );
        btReject= (Button) findViewById( R.id.btReject );

        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            id = Integer.parseInt( extras.getString( "reseller_id" ) );

            name.setText( extras.getString( "reseller_name" ) );
            phone.setText( extras.getString( "reseller_phone" ) );
            email.setText( extras.getString( "reseller_email" ) );

            if(!extras.getString( "reseller_propict" ).equals( "null" )){
                Glide.with( ResellerApprovalActivity.this ).load( APIUtils.BASE_URL + "/show_image/" + extras.getString( "reseller_propict" ) )
                        .crossFade()
                        .thumbnail( 0.5f )
                        .bitmapTransform( new CircleTransform( ResellerApprovalActivity.this ) )
                        .diskCacheStrategy( DiskCacheStrategy.ALL )
                        .into( propict );
            }

        }

        btAccept.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final ProgressDialog spinner = new ProgressDialog( ResellerApprovalActivity.this );
                spinner.setMessage( "Please wait..." );
                spinner.setCancelable( false );
                spinner.show();

                HashMap<String, String> params = new HashMap<>();
                params.put( "api_token", user.get( SessionManager.KEY_TOKEN ) );
                params.put( "child", id + "");

                APIServices apiServices = APIUtils.getAPIService();
                Call<ResponseBody> result = apiServices.acceptRelations( params );
                result.enqueue( new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        spinner.dismiss();

                        try {

                            JSONObject jsonResponse = new JSONObject(response.body().string());
                            boolean status = jsonResponse.getBoolean("status");
                            String message = jsonResponse.getString("message");
                            String child   = jsonResponse.getString( "child" );

                            if(status){

                                Toast.makeText( ResellerApprovalActivity.this, message, Toast.LENGTH_LONG ).show();

                                Intent intent = new Intent( ResellerApprovalActivity.this, MainActivity.class );
                                if(child.equals( "1" )) {
                                    intent.putExtra( "navItemIndex", "8" );
                                    intent.putExtra( "CURRENT_TAG", "reseller" );
                                } else if(child.equals( "2" )){
                                    intent.putExtra( "navItemIndex", "7" );
                                    intent.putExtra( "CURRENT_TAG", "supplier" );
                                }
                                intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
                                startActivity( intent );
                                finish();

                            } else {

                                AlertDialog.Builder builder = new AlertDialog.Builder( ResellerApprovalActivity.this );
                                builder.setMessage( message )
                                        .setNegativeButton( "Retry", null )
                                        .create()
                                        .show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        spinner.dismiss();
                        t.printStackTrace();
                    }
                } );

            }
        } );

        btReject.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final ProgressDialog spinner = new ProgressDialog( ResellerApprovalActivity.this );
                spinner.setMessage( "Please wait..." );
                spinner.setCancelable( false );
                spinner.show();

                HashMap<String, String> params = new HashMap<>();
                params.put( "api_token", user.get( SessionManager.KEY_TOKEN ) );
                params.put( "child", id + "");

                APIServices apiServices = APIUtils.getAPIService();
                Call<ResponseBody> result = apiServices.rejectRelations( params );
                result.enqueue( new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        spinner.dismiss();

                        try {

                            JSONObject jsonResponse = new JSONObject(response.body().string());
                            boolean status = jsonResponse.getBoolean("status");
                            String message = jsonResponse.getString("message");

                            if(status){

                                Toast.makeText( ResellerApprovalActivity.this, message, Toast.LENGTH_LONG );

                                Intent intent = new Intent( ResellerApprovalActivity.this, MainActivity.class );
                                intent.putExtra( "navItemIndex", "4" );
                                intent.putExtra( "CURRENT_TAG", "notifications" );
                                intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
                                startActivity( intent );
                                finish();

                            } else {

                                AlertDialog.Builder builder = new AlertDialog.Builder( ResellerApprovalActivity.this );
                                builder.setMessage( message )
                                        .setNegativeButton( "Retry", null )
                                        .create()
                                        .show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        spinner.dismiss();
                        t.printStackTrace();
                    }
                } );

            }
        } );

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
