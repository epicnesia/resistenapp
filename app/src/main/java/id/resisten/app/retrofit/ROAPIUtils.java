package id.resisten.app.retrofit;

/**
 * Created by LEKTOP on 23-Apr-17.
 */

public class ROAPIUtils {

    private ROAPIUtils(){}

    public static final String RO_BASE_URL = "http://pro.rajaongkir.com/";

    public static ROAPIServices getROAPIService() {

        return RORetrofitClient.getRoClient(RO_BASE_URL).create(ROAPIServices.class);
    }

}
