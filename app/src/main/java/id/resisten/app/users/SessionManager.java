package id.resisten.app.users;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.Settings;

import java.util.HashMap;

import id.resisten.app.users.LoginActivity;

/**
 * Created by LEKTOP on 19-Mar-17.
 */

public class SessionManager {
    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "ResistenPref";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "isLoggedIn";

    // User id
    public static final String KEY_ID = "id";

    // User name
    public static final String KEY_NAME = "name";

    // User store name
    public static final String KEY_STORE_NAME = "storeName";

    // Email address
    public static final String KEY_EMAIL = "email";

    // Phone address
    public static final String KEY_PHONE = "phone";

    // User API token
    public static final String KEY_TOKEN = "api_token";

    // User profile picture
    public static final String KEY_PROPICT = "propict";

    // User province id
    public static final String KEY_PROVINCE_ID = "provinceId";

    // User province name
    public static final String KEY_PROVINCE_NAME = "provinceName";

    // User city id
    public static final String KEY_CITY_ID = "cityId";

    // User city name
    public static final String KEY_CITY_NAME = "cityName";

    // User subdistrict id
    public static final String KEY_SUBDISTRICT_ID = "subdistrictId";

    // User subdistrict name
    public static final String KEY_SUBDISTRICT_NAME = "subdistrictName";

    // User address
    public static final String KEY_ADDRESS = "address";

    // User type, 1 = Reseller, 2 = Supplier
    public static final String KEY_TYPE = "type";

    // Constructor
    public SessionManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session
     * */
    public void createLoginSession(String id, String name, String storeName, String email, String phone, String token,
                                   String propict, String provinceId, String provinceName, String cityId, String cityName,
                                   String subdistrictId, String subdistrictName, String address, String type){

        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        // Storing id in pref
        editor.putString(KEY_ID, id);

        // Storing name in pref
        editor.putString(KEY_NAME, name);

        // Storing store name in pref
        editor.putString(KEY_STORE_NAME, storeName);

        // Storing email in pref
        editor.putString(KEY_EMAIL, email);

        // Storing phone in pref
        editor.putString(KEY_PHONE, phone);

        // Storing token in pref
        editor.putString(KEY_TOKEN, token);

        // Storing profile picture in pref
        editor.putString(KEY_PROPICT, propict);

        // Storing province id in pref
        editor.putString(KEY_PROVINCE_ID, provinceId);

        // Storing province name in pref
        editor.putString(KEY_PROVINCE_NAME, provinceName);

        // Storing city id in pref
        editor.putString(KEY_CITY_ID, cityId);

        // Storing city name in pref
        editor.putString(KEY_CITY_NAME, cityName);

        // Storing subdistrict id in pref
        editor.putString(KEY_SUBDISTRICT_ID, subdistrictId);

        // Storing subdistrict name in pref
        editor.putString(KEY_SUBDISTRICT_NAME, subdistrictName);

        // Storing address in pref
        editor.putString(KEY_ADDRESS, address);

        // Storing type in pref
        editor.putString(KEY_TYPE, type);

        // commit changes
        editor.commit();
    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     * */
    public void checkLogin(){
        // Check login status
        if(!this.isLoggedIn()){
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, LoginActivity.class);

            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);
        }

    }

    /**
     * Get stored session data
     * */
    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();

        // user id
        user.put(KEY_ID, pref.getString(KEY_ID, null));

        // user name
        user.put(KEY_NAME, pref.getString(KEY_NAME, null));

        // user store name
        user.put(KEY_STORE_NAME, pref.getString(KEY_STORE_NAME, null));

        // user email
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));

        // user phone
        user.put(KEY_PHONE, pref.getString(KEY_PHONE, null));

        // user token
        user.put(KEY_TOKEN, pref.getString(KEY_TOKEN, null));

        // user profile picture
        user.put(KEY_PROPICT, pref.getString(KEY_PROPICT, null));

        // user province ID
        user.put(KEY_PROVINCE_ID, pref.getString(KEY_PROVINCE_ID, null));

        // user province name
        user.put(KEY_PROVINCE_NAME, pref.getString(KEY_PROVINCE_NAME, null));

        // user city ID
        user.put(KEY_CITY_ID, pref.getString(KEY_CITY_ID, null));

        // user city name
        user.put(KEY_CITY_NAME, pref.getString(KEY_CITY_NAME, null));

        // user subdistrict ID
        user.put(KEY_SUBDISTRICT_ID, pref.getString(KEY_SUBDISTRICT_ID, null));

        // user subdistrict name
        user.put(KEY_SUBDISTRICT_NAME, pref.getString(KEY_SUBDISTRICT_NAME, null));

        // user address name
        user.put(KEY_ADDRESS, pref.getString(KEY_ADDRESS, null));

        // user type
        user.put(KEY_TYPE, pref.getString(KEY_TYPE, null));

        // return user
        return user;
    }

    /**
     * Clear session details
     * */
    public void logoutUser(){
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect user to Login Activity
        Intent i = new Intent(_context, LoginActivity.class);

        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        _context.startActivity(i);
    }

    /**
     * Quick check for login
     * **/
    // Get Login State
    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }
}
