package id.resisten.app.retrofit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by LEKTOP on 23-Apr-17.
 */

public class RORetrofitClient {

    private static Retrofit roRetrofit;

    static Retrofit getRoClient(String baseUrl) {
        if(roRetrofit==null){
            roRetrofit = new Retrofit.Builder()
                    .baseUrl( baseUrl )
                    .addConverterFactory( GsonConverterFactory.create() )
                    .build();
        }

        return roRetrofit;
    }
}
