package id.resisten.app.resellers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import id.resisten.app.R;

import static android.content.ContentValues.TAG;

/**
 * Created by LEKTOP on 19-May-17.
 */

public class ResellerDetailsAdapter extends RecyclerView.Adapter<ResellerDetailsAdapter.ViewHolder> {

    private Context context;
    private List<ResellerDetailsModel> resellersList;

    public ResellerDetailsAdapter(Context context, List<ResellerDetailsModel> resellersList) {
        this.context = context;
        this.resellersList = resellersList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from( parent.getContext() ).inflate( R.layout.activity_reseller_details_rv, parent, false );
        return new ViewHolder( itemView );
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        DecimalFormat IndonesianCurrency = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol( "Rp. " );
        formatRp.setMonetaryDecimalSeparator( ',' );
        formatRp.setGroupingSeparator( '.' );

        IndonesianCurrency.setDecimalFormatSymbols( formatRp );

        String orderID = "#" + resellersList.get( position ).getId();

        holder.tvOrderId.setText( orderID );
        holder.tvOrderDate.setText( formatDateFromString( "yyyy-MM-dd HH:mm:ss", "dd-MMM-yyyy HH:mm:ss", resellersList.get( position ).getDate() ) );
        holder.tvSupplierName.setText( resellersList.get( position ).getSupplierName() );
        holder.tvProductName.setText( resellersList.get( position ).getProductName() );
        holder.tvGrandTotal.setText( IndonesianCurrency.format( resellersList.get( position ).getGrandTotal() ) );
        holder.tvOrderStatus.setText( resellersList.get( position ).getStatus() );
    }

    @Override
    public int getItemCount() {
        return resellersList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvOrderId,
                tvOrderDate,
                tvSupplierName,
                tvProductName,
                tvGrandTotal,
                tvOrderStatus;

        public ViewHolder(View itemView) {
            super( itemView );

            tvOrderId       = (TextView) itemView.findViewById( R.id.tvResellerDetailsOrderId );
            tvOrderDate     = (TextView) itemView.findViewById( R.id.tvResellerDetailsOrderDate );
            tvSupplierName  = (TextView) itemView.findViewById( R.id.tvResellerDetailsSuppName );
            tvProductName   = (TextView) itemView.findViewById( R.id.tvResellerDetailsProductName );
            tvGrandTotal    = (TextView) itemView.findViewById( R.id.tvResellerDetailsGrandTotal );
            tvOrderStatus   = (TextView) itemView.findViewById( R.id.tvResellerDetailsOrderStatus );
        }
    }

    public static String formatDateFromString(String inputFormat, String outputFormat, String inputDate){

        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);

        } catch (ParseException e) {
            Log.e(TAG, "ParseException - dateFormat");
        }

        return outputDate;

    }
}
