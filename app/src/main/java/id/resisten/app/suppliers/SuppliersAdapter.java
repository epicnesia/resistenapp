package id.resisten.app.suppliers;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import id.resisten.app.R;
import id.resisten.app.retrofit.APIUtils;
import id.resisten.app.users.CircleTransform;

/**
 * Created by LEKTOP on 19-Apr-17.
 */

public class SuppliersAdapter extends RecyclerView.Adapter<SuppliersAdapter.ViewHolder> {

    private Context context;
    private List<SuppliersModel> supplierList;

    public SuppliersAdapter(Context context, List<SuppliersModel> supplierList) {
        this.context = context;
        this.supplierList = supplierList;
    }

    @Override
    public SuppliersAdapter.ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from( parent.getContext() ).inflate( R.layout.fragment_supplier_rv, parent, false );
        return new SuppliersAdapter.ViewHolder( itemView );
    }

    @Override
    public void onBindViewHolder(SuppliersAdapter.ViewHolder holder, final int position) {

        if(!supplierList.get( position ).getImage().equals("null")) {
            Glide.with( context ).load( APIUtils.BASE_URL + "/show_image/" + supplierList.get( position ).getImage() )
                    .crossFade()
                    .thumbnail( 0.5f )
                    .bitmapTransform( new CircleTransform( context ) )
                    .diskCacheStrategy( DiskCacheStrategy.ALL )
                    .into( holder.propict );
        }

        holder.userName.setText( supplierList.get( position ).getName() );

        if( supplierList.get( position ).getRelationStatus() == 2 ){
            holder.actionButton.setBackgroundResource( R.color.colorPrimary );
            holder.actionButton.setText( R.string.chat );
            holder.actionButton.setTextColor( Color.WHITE );
        }

        holder.rlSupplier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = supplierList.get( position ).getId();

                Intent intent = new Intent(context, SupplierDetailsActivity.class);
                intent.putExtra( "supplier_id", id + "" );
                intent.putExtra( "related", "1" );
                context.startActivity( intent );
            }
        });

    }

    @Override
    public int getItemCount() {
        return supplierList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public RelativeLayout rlSupplier;
        public ImageView propict;
        public TextView userName;
        public Button actionButton;

        public ViewHolder(View itemView) {
            super( itemView );

            rlSupplier  = (RelativeLayout) itemView.findViewById( R.id.rlSupplier );
            propict     = (ImageView) itemView.findViewById( R.id.ivSupplierPropict );
            userName    = (TextView) itemView.findViewById( R.id.tvSupplierName );
            actionButton= (Button) itemView.findViewById( R.id.btActionSupp );
        }
    }

}
