package id.resisten.app.notifications;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import id.resisten.app.R;
import id.resisten.app.users.LoginActivity;
import id.resisten.app.users.SessionManager;

public class NotificationsFragment extends Fragment {

    // Session Manager class
    private SessionManager session;
    private HashMap<String, String> user;

    private TabLayout tabLayout;
    private ViewPager viewPager;

    public NotificationsFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );

        // Session manager instance
        session = new SessionManager(getActivity().getApplicationContext());
        if(!session.isLoggedIn()){
            Intent loginIntent = new Intent(this.getActivity(), LoginActivity.class);
            this.getActivity().startActivity(loginIntent);
            getActivity().finish();
        }

        user = session.getUserDetails();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate( R.layout.fragment_notifications, container, false );

        viewPager = (ViewPager) view.findViewById(R.id.notifViewPager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) view.findViewById(R.id.notifTabLayout );
        tabLayout.setupWithViewPager(viewPager);
        return view;

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new NewOrderFragment(), getResources().getString( R.string.navOrder ));
        adapter.addFragment(new CoopRequestFragment(), getResources().getString( R.string.request ));
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
