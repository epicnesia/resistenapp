package id.resisten.app.retrofit;

import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by LEKTOP on 23-Apr-17.
 */

public interface ROAPIServices {

    // Get provinces list
    @Headers({"Accept: application/json", "Content-Type: application/json", "key: b61074ec862c34d910e3e11c0b38c29b"})
    @GET("/api/province")
    Call<ResponseBody> getProvinces();

    // Get cities list
    @Headers({"Accept: application/json", "Content-Type: application/json", "key: b61074ec862c34d910e3e11c0b38c29b"})
    @GET("/api/city")
    Call<ResponseBody> getCities(@Query( "province" ) int provinceId);

    // Get subdistricts list
    @Headers({"Accept: application/json", "Content-Type: application/json", "key: b61074ec862c34d910e3e11c0b38c29b"})
    @GET("/api/subdistrict")
    Call<ResponseBody> getSubdistrict(@Query( "city" ) int cityId);

    // Get shipping cost
    @Headers({"Accept: application/json", "key: b61074ec862c34d910e3e11c0b38c29b"})
    @FormUrlEncoded
    @POST("/api/cost")
    Call<ResponseBody> getShippingCost(@FieldMap HashMap<String, String> params);
}
